import 'package:firebase_core/firebase_core.dart';

import 'package:fixup/app/login/authentication.dart';
import 'package:fixup/constants/colors.dart';
import 'package:fixup/models/media_model.dart';
import 'package:fixup/models/report_model.dart';
import 'package:fixup/models/location_model.dart';
import 'package:flutter/material.dart';
import 'package:fixup/constants/keys.dart';
import 'package:hive/hive.dart';
import 'package:hive_flutter/hive_flutter.dart';

Future<void> main() async {
  WidgetsFlutterBinding.ensureInitialized();
  await Firebase.initializeApp();
  await Hive.initFlutter();
  Hive.registerAdapter(LocationAdapter());
  Hive.registerAdapter(MediaAdapter());
  Hive.registerAdapter(ReportModelAdapter());
  await Hive.openBox<ReportModel>('reports_box');
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: Keys.appName,
      theme: ThemeData(
        primarySwatch: Palette
            .orangeToLight, //our palette goes here by tapping into the class
      ),
      home: const Auth(),
    );
  }
}
