import 'dart:collection';
import 'dart:io';
import 'package:fixup/constants/strings.dart';
import 'package:hive/hive.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:uuid/uuid.dart';
import 'package:firebase_storage/firebase_storage.dart';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:cloud_functions/cloud_functions.dart';
import 'package:fixup/models/report_model.dart';

class ReportApiProvider {
  CollectionReference reports =
      FirebaseFirestore.instance.collection('reports');

  var storage = FirebaseStorage.instance;

  addOne(report, image) async {
    String fileID = Uuid().v4();
    String cat = report["category"];
    TaskSnapshot snapshot =
        await storage.ref().child("images/$fileID").putFile(File(image.path));
    if (snapshot.state == TaskState.success) {
      final String downloadUrl = await snapshot.ref.getDownloadURL();
      reports.add({
        "author": report["author"],
        "severity": report["severity"],
        "category": report["category"],
        "description": report["description"],
        "media": {
          "type": "PHOTO",
          "path": downloadUrl,
          "altName": "Photo report of type $cat"
        },
        "damageLocation": {
          "latitude": report["latitude_damage"],
          "longitude": report["longitude_damage"]
        },
        "creationLocation": {
          "latitude": report["latitude"],
          "longitude": report["longitude"]
        },
        "status": "Active",
        "type": report["type"],
        "numVotes": 0,
        "city": "Bogota",
        "country": "Colombia",
        "created": FieldValue.serverTimestamp(),
        "timer": report["timer"]
      }).then((value) {
        print(value.id);
      }).timeout(const Duration(seconds: 7));
    }
  }

  vote(reportId) async {
    reports
        .doc(reportId) // <-- Doc ID where data should be updated.
        .update({'numVotes': FieldValue.increment(1)}) // <-- Updated data
        .then((_) => print("updated: " + reportId))
        .catchError((error) => print('Update failed: $error'))
        .timeout(const Duration(seconds: 7));
  }

  Future<ReportModel> getById(docId) async {
    String errorMessage;
    try {
      var value =
          await reports.doc(docId).get().timeout(const Duration(seconds: 7));
      var report = ReportModel.fromSnapshot(value);
      return report;
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }

  Future<YourReportsModel> getYourReports(author) async {
    String errorMessage;
    try {
      HttpsCallable callable =
          FirebaseFunctions.instance.httpsCallable('dashboardYourReports');
      final results = await callable.call(
        <String, dynamic>{
          'author': author,
        },
      ).timeout(const Duration(seconds: 7));

      YourReportsModel dashboardData =
          YourReportsModel.fromJson(HashMap.from(results.data));

      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setInt('reportsMade', dashboardData.reportsMade);
      await prefs.setInt(
          'daysSinceLastReport', dashboardData.daysSinceLastReport);
      await prefs.setDouble(
          'repairedYourReports', dashboardData.repairPercentage);
      await prefs.setDouble(
          'repairTimeYourReports', dashboardData.averageRepairTime);
      await prefs.setDouble(
          'averageVotesReportsYourReports', dashboardData.averageVotesReports);

      return dashboardData;
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }

  Future<OverallReportsModel> getOverallReports(city) async {
    String errorMessage;
    try {
      HttpsCallable callable =
          FirebaseFunctions.instance.httpsCallable('dashboardOverallReports');
      final results = await callable.call(
        <String, dynamic>{
          'city': city,
        },
      ).timeout(const Duration(seconds: 7));
      OverallReportsModel dashboardData =
          OverallReportsModel.fromJson(HashMap.from(results.data));
      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setDouble('votesPerRepair', dashboardData.averageVotes);
      await prefs.setDouble('repairedOverall', dashboardData.repairPercentage);
      await prefs.setDouble(
          'repairTimeOverall', dashboardData.averageRepairTime);
      await prefs.setInt(
          'mostVotedCategoryVotes', dashboardData.mostVotedCategoryVotes);
      await prefs.setString(
          'mostVotedCategory', dashboardData.mostVotedCategory);
      return dashboardData;
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }

  Future<YourVotesModel> getYourVotes(author) async {
    String errorMessage;
    try {
      HttpsCallable callable =
          FirebaseFunctions.instance.httpsCallable('dashboardYourVotes');

      final results = await callable.call(
        <String, dynamic>{
          'user': author,
        },
      ).timeout(const Duration(seconds: 7));
      YourVotesModel dashboardData =
          YourVotesModel.fromJson(HashMap.from(results.data));

      SharedPreferences prefs = await SharedPreferences.getInstance();
      await prefs.setInt('reportsVoted', dashboardData.reportsVoted);
      await prefs.setDouble(
          'repairedYourVotes', dashboardData.repairPercentage);
      await prefs.setDouble(
          'repairTimeYourVotes', dashboardData.averageRepairTime);
      await prefs.setString(
          'mostVotedCategoryYourVotes', dashboardData.mostVotedCategory);
      await prefs.setInt(
          'mostVotedVotesYourVotes', dashboardData.mostVotedVotes);
      return dashboardData;
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }

  Future<List<ReportModel>> getReportsBetweenCoordinates(bool updateDB,
      [LatLngBounds? mapMarginsLimit]) async {
    Box<ReportModel> box = await Hive.box("reports_box");

    double northeastLatitude;
    double northeastLongitude;
    double southwestLatitude;
    double southwestLongitude;
    if (mapMarginsLimit == null) {
      northeastLatitude = 4.712226043216751;
      northeastLongitude = -74.06133368611336;
      southwestLatitude = 4.68998480549401;
      southwestLongitude = -74.07678354531527;
    } else {
      northeastLatitude = mapMarginsLimit.northeast.latitude;
      northeastLongitude = mapMarginsLimit.northeast.longitude;
      southwestLatitude = mapMarginsLimit.southwest.latitude;
      southwestLongitude = mapMarginsLimit.southwest.longitude;
    }
    String errorMessage;

    try {
      HttpsCallable callable =
          FirebaseFunctions.instance.httpsCallable('filterPoints');
      final results = await callable.call(
        <String, dynamic>{
          'northeastLatitude': northeastLatitude,
          'northeastLongitude': northeastLongitude,
          'southwestLatitude': southwestLatitude,
          'southwestLongitude': southwestLongitude
        },
      ).timeout(const Duration(seconds: 7));

      List<ReportModel> documents = [];
      List<dynamic> filteredPoints =
          HashMap.from(results.data)["filteredPoints"];
      if (updateDB) {
        await box.clear();
      }
      for (var doc in filteredPoints) {
        ReportModel report = ReportModel.fromJson(doc);
        documents.add(report);
        if (updateDB) {
          box.add(report);
        }
      }

      return documents;
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }

  Future<List<ReportModel>> getReportsSmartFeature(
      userPosition, distance) async {
    double latitude = userPosition.latitude;
    double longitude = userPosition.longitude;

    String errorMessage;
    try {
      HttpsCallable callable =
          FirebaseFunctions.instance.httpsCallable('smartFeature');
      final results = await callable.call(
        <String, dynamic>{
          'distance': distance,
          'userLatitude': latitude,
          'userLongitude': longitude
        },
      ).timeout(const Duration(seconds: 7));

      List<ReportModel> documents = [];
      List<dynamic> filteredPoints =
          HashMap.from(results.data)["filteredPoints"];
      for (var doc in filteredPoints) {
        ReportModel report = ReportModel.fromJson(doc);
        documents.add(report);
        print(report);
      }
      return documents;
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }

  Future<List<ReportModel>> getAll() async {
    var query = await reports.get().timeout(const Duration(seconds: 7));
    List<ReportModel> documents = [];
    for (var doc in query.docs) {
      ReportModel report = ReportModel.fromSnapshot(doc);
      documents.add(report);
    }
    return documents;
  }

  Future<List<ReportModel>> getAllForUser(author) async {
    String errorMessage;
    try {
      var query = await reports
          .where("author", isEqualTo: author)
          .get()
          .timeout(const Duration(seconds: 7));
      List<ReportModel> documents = [];
      for (var doc in query.docs) {
        ReportModel report = ReportModel.fromSnapshot(doc);
        documents.add(report);
      }
      return documents;
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }
}
