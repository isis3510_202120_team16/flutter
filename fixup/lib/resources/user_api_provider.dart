import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fixup/models/user_model.dart';

class UserApiProvider {
  FirebaseAuth auth = FirebaseAuth.instance;
  CollectionReference users = FirebaseFirestore.instance.collection('users');

  Future<UserCredential> signUpWithCredentials(
      String email, String password) async {
    UserCredential userCredential = await auth.createUserWithEmailAndPassword(
        email: email, password: password);
    return userCredential;
  }

  Future<UserCredential> signInWithCredentials(String email, String password) {
    return auth.signInWithEmailAndPassword(email: email, password: password);
  }

  bool isSignedIn() {
    final currentUser = auth.currentUser;
    return currentUser != null;
  }

  User? getUser() {
    final user = auth.currentUser;

    return user;
  }

  Future<void> signOut() async {
    return await (auth.signOut());
  }

  addVotedReport(reportId) async {
    var uid = auth.currentUser!.uid;
    try {
      final snapShot = await users.doc(uid).get();

      if (snapShot.exists) {
        users.doc(uid).update({
          "votedReports": FieldValue.arrayUnion([reportId])
        }).timeout(const Duration(seconds: 7));
      } else {
        users.doc(uid).set({
          "votedReports": [reportId]
        });
      }
    } catch (e) {
      // TODO: Do something clever.
      print("error");
    }
  }

  Future<bool> isReportVoted(reportId) async {
    var uid = auth.currentUser!.uid;
    try {
      final snapShot =
          await users.doc(uid).get().timeout(const Duration(seconds: 7));
      if (snapShot.exists) {
        var voted = UserModel.fromSnapshot(snapShot);

        if (voted.votedReports.contains(reportId)) {
          return true;
        }

        return false;
      } else {
        return false;
      }
    } catch (e) {
      // TODO: Do something clever.
      print(e);
    }
    return false;
  }
}
