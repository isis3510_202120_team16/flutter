import 'package:cloud_firestore/cloud_firestore.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';

import 'package:fixup/models/report_model.dart';
import 'package:fixup/resources/report_api_provider.dart';
import 'package:fixup/resources/report_dao.dart';
import 'package:fixup/resources/user_api_provider.dart';

class Repository {
  final reportsApiProvider = ReportApiProvider();
  final userApiProvider = UserApiProvider();

  final reportsDao = ReportDao();

  Future<ReportModel> getById(docId) {
    return reportsApiProvider.getById(docId).catchError((err) {
      return reportsDao.getDetail(docId);
    });
  }

  Future<List<ReportModel>> getAll() => reportsApiProvider.getAll();

  Future<List<ReportModel>> getReportsBetweenCoordinates(
      LatLngBounds? mapMarginsLimit, bool updateDB) {
    return reportsApiProvider
        .getReportsBetweenCoordinates(updateDB, mapMarginsLimit)
        .catchError((err) {
      return reportsDao.getReportsDB();
    });
  }

  Future<List<ReportModel>> getReportsSmartFeature(userPosition, distance) =>
      reportsApiProvider.getReportsSmartFeature(userPosition, distance);

  Future<List<ReportModel>> getAllForUser(author) {
    return reportsApiProvider.getAllForUser(author);
  }

  Future<YourReportsModel> getYourReports(author) {
    return reportsApiProvider.getYourReports(author).catchError((err) {
      return reportsDao.getYourReports(author);
    });
  }

  Future<OverallReportsModel> getOverallReports(city) {
    return reportsApiProvider.getOverallReports(city).catchError((err) {
      return reportsDao.getOverallReports();
    });
  }

  Future<YourVotesModel> getYourVotes(author) {
    return reportsApiProvider.getYourVotes(author).catchError((err) {
      return reportsDao.getYourVotes(author);
    });
  }

  createReport(report, image) => reportsApiProvider.addOne(report, image);
  vote(reportId) => reportsApiProvider.vote(reportId);

  signIn(email, password) async {
    return await userApiProvider.signInWithCredentials(email, password);
  }

  getUser() {
    return userApiProvider.getUser();
  }

  signOut() {
    userApiProvider.signOut();
  }

  isSignedIn() {
    userApiProvider.isSignedIn();
  }

  addVotedReport(reportId) {
    userApiProvider.addVotedReport(reportId);
  }

  Future<bool> isReportVoted(reportId) {
    return userApiProvider.isReportVoted(reportId);
  }
}
