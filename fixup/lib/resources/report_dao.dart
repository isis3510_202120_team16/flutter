import 'package:fixup/constants/strings.dart';
import 'package:fixup/models/media_model.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:hive/hive.dart';
import 'package:fixup/models/report_model.dart';

class ReportDao {
  ///////////////////////////////////////////////////////
  ///////////////Prefereces/////////////////////////////

  Future<OverallReportsModel> getOverallReports() async {
    String errorMessage;
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      double? votesPerRepair = prefs.getDouble('votesPerRepair');
      double? repairedOverall = prefs.getDouble('repairedOverall');
      double? repairTimeOverall = prefs.getDouble('repairTimeOverall');
      int? votes = prefs.getInt('mostVotedCategoryVotes');
      String? category = prefs.getString('mostVotedCategory');

      if (votesPerRepair != null &&
          repairedOverall != null &&
          repairTimeOverall != null) {
        OverallReportsModel dashboardData = OverallReportsModel.fromJson({
          "averageVotes": votesPerRepair,
          "repairPercentage": repairedOverall,
          "averageRepairTime": repairTimeOverall,
          "votes": votes,
          "category": category
        });
        return dashboardData;
      } else {
        errorMessage = Strings.daoDashboardError;
      }
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }

  Future<YourReportsModel> getYourReports(author) async {
    String errorMessage;
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int? reportsMade = prefs.getInt('reportsMade');
      int? daysSinceLastReport = prefs.getInt('daysSinceLastReport');
      double? repairPercentage = prefs.getDouble('repairedYourReports');
      double? repairTime = prefs.getDouble('repairTimeYourReports');
      double? averageVotesReports =
          prefs.getDouble('averageVotesReportsYourReports');

      if (reportsMade != null &&
          daysSinceLastReport != null &&
          repairPercentage != null &&
          repairTime != null &&
          averageVotesReports != null) {
        YourReportsModel data = YourReportsModel.fromJson({
          "reportsMade": reportsMade,
          "daysSinceLastReport": daysSinceLastReport,
          "repairPercentage": repairPercentage,
          "averageRepairTime": repairTime,
          "averageVotesReports": averageVotesReports
        });
        return data;
      } else {
        errorMessage = Strings.daoDashboardError;
      }
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }

  Future<YourVotesModel> getYourVotes(author) async {
    String errorMessage;
    try {
      SharedPreferences prefs = await SharedPreferences.getInstance();
      int? reportsVoted = prefs.getInt('reportsVoted');
      double? repairPercentage = prefs.getDouble('repairedYourVotes');
      double? repairTime = prefs.getDouble('repairTimeYourVotes');
      String? category = prefs.getString('mostVotedCategoryYourVotes');
      int? votes = prefs.getInt('mostVotedVotesYourVotes');

      if (reportsVoted != null &&
          repairPercentage != null &&
          repairTime != null) {
        YourVotesModel data = YourVotesModel.fromJson({
          "reportsVoted": reportsVoted,
          "repairPercentage": repairPercentage,
          "averageRepairTime": repairTime,
          "category": category,
          "votes": votes
        });
        return data;
      } else {
        errorMessage = Strings.daoDashboardError;
      }
    } catch (error) {
      errorMessage = Strings.unknownError;
    }
    return Future.error(errorMessage);
  }

  ////////////////////////////////////////////////////////////////////////////////
  //////////////////////////////LOCAL DB////////////////////////////////////////////////

  Future<List<ReportModel>> getReportsDB() async {
    Box<ReportModel> box = await Hive.box("reports_box");
    return box.values.toList();
  }

  Future<ReportModel> getDetail(firebaseId) async {
    Box<ReportModel> box = await Hive.box("reports_box");
    return box.values.where((item) => item.id == firebaseId).toList()[0];
  }
}
