import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fixup/models/location_model.dart';
import 'package:fixup/models/media_model.dart';
import 'package:hive/hive.dart';
part 'report_model.g.dart';

@HiveType(typeId: 1)
class ReportModel extends HiveObject {
  @HiveField(0)
  String type;
  @HiveField(1)
  String city;
  @HiveField(2)
  String country;
  @HiveField(3)
  String category;
  @HiveField(4)
  String description;
  @HiveField(5)
  String status;
  @HiveField(6)
  String id;
  //String createTime, repairTime;
  @HiveField(7)
  int severity;
  @HiveField(8)
  int numVotes;
  @HiveField(9)
  List<Media> media = [];
  @HiveField(10)
  Location damageLocation;
  @HiveField(11)
  Location createLocation;

  ReportModel(
      this.id,
      this.type,
      this.city,
      this.country,
      this.category,
      this.description,
      this.status,
      this.severity,
      this.numVotes,
      this.media,
      this.damageLocation,
      this.createLocation);

  factory ReportModel.fromSnapshot(DocumentSnapshot docSnap) {
    List<Media> listMedia = [Media.fromObject(docSnap.get('media'))];
    Location damage = Location.fromObject(docSnap.get('damageLocation'));
    Location create = Location.fromObject(docSnap.get('creationLocation'));

    return ReportModel(
        docSnap.reference.id,
        docSnap.get('type'),
        docSnap.get('city'),
        docSnap.get('country'),
        docSnap.get('category'),
        docSnap.get('description'),
        docSnap.get('status'),
        docSnap.get('severity'),
        docSnap.get('numVotes'),
        listMedia,
        damage,
        create);
  }

  factory ReportModel.fromJson(json) {
    Location damageLocation = Location(json["damageLocation"]["latitude"],
        json["damageLocation"]["longitude"]);
    Location createLocation = Location(json["creationLocation"]["latitude"],
        json["creationLocation"]["longitude"]);
    List<Media> listMedia = [Media.fromObject(json['media'])];
    return ReportModel(
        json["id"],
        json["type"],
        json["city"],
        json["country"],
        json["category"],
        json["description"],
        json["status"],
        json["severity"],
        json["numVotes"],
        listMedia,
        damageLocation,
        createLocation);
  }
}

class YourReportsModel {
  int reportsMade, daysSinceLastReport;
  double repairPercentage, averageRepairTime, averageVotesReports;

  YourReportsModel(this.reportsMade, this.daysSinceLastReport,
      this.repairPercentage, this.averageRepairTime, this.averageVotesReports);

  factory YourReportsModel.fromJson(json) {
    int days = json["daysSinceLastReport"].floor();
    double repaired = json["repairPercentage"].toDouble();
    double average = json["averageRepairTime"].toDouble();
    double averageVotes = json["averageVotesReports"].toDouble();
    if (average == -1) {
      average = 0.0;
    }

    return YourReportsModel(
        json["reportsMade"], days, repaired, average, averageVotes);
  }
}

class OverallReportsModel {
  double averageVotes, repairPercentage, averageRepairTime;
  int mostVotedCategoryVotes;
  String mostVotedCategory;

  OverallReportsModel(
      this.averageVotes,
      this.repairPercentage,
      this.averageRepairTime,
      this.mostVotedCategoryVotes,
      this.mostVotedCategory);

  factory OverallReportsModel.fromJson(json) {
    double averageVotes = json["averageVotes"].toDouble();
    double repaired = json["repairPercentage"].toDouble();
    double averageRepairTime = json["averageRepairTime"].toDouble();

    if (averageVotes == -1) {
      averageVotes = 0.0;
    }

    if (averageRepairTime == -1) {
      averageRepairTime = 0.0;
    }

    return OverallReportsModel(averageVotes, repaired, averageRepairTime,
        json["votes"], json["category"]);
  }
}

class YourVotesModel {
  String mostVotedCategory;
  int reportsVoted, mostVotedVotes;
  double repairPercentage, averageRepairTime;

  YourVotesModel(this.reportsVoted, this.repairPercentage,
      this.averageRepairTime, this.mostVotedVotes, this.mostVotedCategory);

  factory YourVotesModel.fromJson(json) {
    int reportsVoted = json["reportsVoted"].floor();
    double repaired = json["repairPercentage"].toDouble();
    double averageRepairTime = json["averageRepairTime"].toDouble();
    int votes = json["votes"].floor();

    if (averageRepairTime == -1) {
      averageRepairTime = 0.0;
    }

    return YourVotesModel(
        reportsVoted, repaired, averageRepairTime, votes, json["category"]);
  }
}
