import 'package:hive/hive.dart';
part 'location_model.g.dart';

@HiveType(typeId: 3)
class Location {
  @HiveField(0)
  double latitude;
  @HiveField(1)
  double longitude;

  Location(this.latitude, this.longitude);

  factory Location.fromObject(object) {
    Map data = object;
    return Location(data["latitude"], data["longitude"]);
  }
}
