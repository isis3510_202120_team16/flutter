import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:fixup/models/location_model.dart';

class UserModel {
  String id;

  List<String> votedReports = [];

  UserModel(this.id, this.votedReports);

  factory UserModel.fromSnapshot(DocumentSnapshot docSnap) {
    List<String> listReports = (docSnap.get('votedReports') as List)
        .map((item) => item as String)
        .toList();
    ;

    return UserModel(docSnap.reference.id, listReports);
  }
}
