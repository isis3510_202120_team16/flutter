import 'package:hive/hive.dart';
part 'media_model.g.dart';

@HiveType(typeId: 2)
class Media {
  @HiveField(0)
  String type;
  @HiveField(1)
  String path;
  @HiveField(2)
  String altName;

  Media(this.type, this.path, this.altName);

  factory Media.fromObject(object) {
    Map data = object;
    return Media(data["type"], data["path"], data["altName"]);
  }
}
