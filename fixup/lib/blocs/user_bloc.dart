import 'package:firebase_auth/firebase_auth.dart';
import 'package:rxdart/rxdart.dart';
import 'package:fixup/models/user_model.dart';
import '../resources/repository.dart';

class UserBloc {
  final _repository = Repository();
  final _userFetcher = PublishSubject<UserModel>();
  final _userReporFetcher = PublishSubject<bool>();

  Stream<UserModel> get user => _userFetcher.stream;
  Stream<bool> get isReport => _userReporFetcher.stream;

  Future<UserCredential> signInUser(String email, String password) async {
    return _repository.userApiProvider.signInWithCredentials(email, password);
  }

  Future<UserCredential> signUpUser(String email, String password) async {
    return _repository.userApiProvider.signUpWithCredentials(email, password);
  }

  User? getUser() {
    return _repository.userApiProvider.getUser();
  }

  signOut() {
    _repository.userApiProvider.signOut();
  }

  isSIgnedIn() {
    return _repository.userApiProvider.isSignedIn();
  }

  addVotedReport(reportId) {
    return _repository.addVotedReport(reportId);
  }

  Future<bool> isReportVoted(reportId) async {
    return await _repository.isReportVoted(reportId);
    //_userReporFetcher.sink.add(isReport);
  }
}

final userBloc = UserBloc();
