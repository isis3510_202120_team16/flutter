import 'dart:async';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:rxdart/rxdart.dart';

import 'package:fixup/models/report_model.dart';
import '../resources/repository.dart';

class ReportsBloc {
  final _repository = Repository();
  final _reportDetailFetcher = PublishSubject<ReportModel>();
  final _reportsFetcher = PublishSubject<List<ReportModel>>();
  final _userReportsFetcher = PublishSubject<List<ReportModel>>();
  final _dashboardYourReports = PublishSubject<YourReportsModel>();
  final _dashboardOverallReports = PublishSubject<OverallReportsModel>();
  final _dashboardYourVotes = PublishSubject<YourVotesModel>();

  Stream<ReportModel> get report => _reportDetailFetcher.stream;
  Stream<List<ReportModel>> get reports => _reportsFetcher.stream;
  Stream<List<ReportModel>> get userReports => _userReportsFetcher.stream;
  Stream<YourReportsModel> get dashboardData => _dashboardYourReports.stream;
  Stream<OverallReportsModel> get overallReports =>
      _dashboardOverallReports.stream;
  Stream<YourVotesModel> get yourVotes => _dashboardYourVotes.stream;

  getById(reportId) async {
    ReportModel reportModel = await _repository.getById(reportId);
    _reportDetailFetcher.sink.add(reportModel);
  }

  getAll() async {
    try {
      List<ReportModel> reportModel = await _repository.getAll();
      _reportsFetcher.sink.add(reportModel);
    } catch (error) {
      _reportsFetcher.addError(error);
    }
  }

  getReportsBetweenCoordinates(bool updateDB,
      [LatLngBounds? mapMarginsLimit]) async {
    try {
      List<ReportModel> reportModel = await _repository
          .getReportsBetweenCoordinates(mapMarginsLimit, updateDB);
      _reportsFetcher.sink.add(reportModel);
    } catch (error) {
      _reportsFetcher.addError(error);
    }
  }

  getReportsSmartFeature(userPosition, distance) async {
    try {
      List<ReportModel> reportModel =
          await _repository.getReportsSmartFeature(userPosition, distance);
      _reportsFetcher.sink.add(reportModel);
    } catch (error) {
      _reportsFetcher.addError(error);
    }
  }

  getAllForUser(author) async {
    try {
      List<ReportModel> reports = await _repository.getAllForUser(author);
      _userReportsFetcher.sink.add(reports);
    } catch (error) {
      _userReportsFetcher.addError(error);
    }
  }

  getYourReports(author) async {
    try {
      YourReportsModel data = await _repository.getYourReports(author);
      _dashboardYourReports.sink.add(data);
    } catch (error) {
      _dashboardYourReports.addError(error);
    }
  }

  getOverallReports(city) async {
    try {
      OverallReportsModel data = await _repository.getOverallReports(city);
      _dashboardOverallReports.sink.add(data);
    } catch (error) {
      _dashboardOverallReports.addError(error);
    }
  }

  getYourVotes(author) async {
    try {
      YourVotesModel data = await _repository.getYourVotes(author);
      _dashboardYourVotes.sink.add(data);
    } catch (error) {
      _dashboardYourVotes.addError(error);
    }
  }

  createReport(report, image) async {
    await _repository.createReport(report, image);
  }

  voteReport(reportId) async {
    await _repository.vote(reportId);
  }

  dispose() {
    /**
    _reportsFetcher.close();
    _reportDetailFetcher.close();
    _userReportsFetcher.close();
    _dashboardYourReports.close();
    _dashboardOverallReports.close();
    _dashboardYourVotes.close();
     */
  }
}

final reportsBloc = ReportsBloc();
