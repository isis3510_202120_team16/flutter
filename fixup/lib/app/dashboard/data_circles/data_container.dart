import 'package:fixup/constants/colors.dart';
import 'package:flutter/material.dart';

class DataContainer extends StatelessWidget {
  final String text;
  final String value;
  final double size;
  // ignore: use_key_in_widget_constructors
  const DataContainer(
      {required this.text, required this.value, required this.size});

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: const EdgeInsets.all(8.0),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          CircleAvatar(
            radius: size,
            backgroundColor: const Color(CustomColors.orange),
            child: Text(
              value,
              textAlign: TextAlign.center,
              style: const TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 20),
            ),
          ),
          Container(
            constraints: BoxConstraints(minWidth: 10, maxWidth: size * 2),
            child: Text(
              text,
              textAlign: TextAlign.center,
            ),
          )
        ]));
  }
}
