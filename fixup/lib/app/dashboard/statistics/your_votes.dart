import 'package:firebase_auth/firebase_auth.dart';
import 'package:fixup/app/generic_fallback.dart';
import 'package:flutter/material.dart';

import 'package:fixup/blocs/reports_bloc.dart';
import 'package:fixup/blocs/user_bloc.dart';
import 'package:fixup/models/report_model.dart';
import 'package:fixup/constants/strings.dart';
import 'package:fixup/app/dashboard/data_circles/data_container.dart';

class YourVotes extends StatefulWidget {
  const YourVotes({Key? key}) : super(key: key);

  @override
  _YourVotesState createState() => _YourVotesState();
}

class _YourVotesState extends State<YourVotes> {
  final double size = 35;
  @override
  void initState() {
    super.initState();
    User? user = userBloc.getUser();
    reportsBloc.getYourVotes(user?.uid);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Row(
                  children: const [
                    Text(Strings.yourVotesCardTitle,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                  ],
                ),
                StreamBuilder(
                  stream: reportsBloc.yourVotes,
                  builder: (context, AsyncSnapshot<YourVotesModel> snapshot) {
                    if (snapshot.hasData) {
                      int reportsVoted = snapshot.data!.reportsVoted;
                      int repaired =
                          (snapshot.data!.repairPercentage * 100).ceil();
                      int averageRepairTime =
                          snapshot.data!.averageRepairTime.floor();
                      int mostVotes = snapshot.data!.mostVotedVotes.floor();
                      if (reportsVoted == -1) {
                        return const SizedBox(
                            height: 110.0,
                            child: Text(
                              "To see some information about your votes you need to vote one of the created reports",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Color(0xff2f3640),
                              ),
                            ));
                      }

                      return SizedBox(
                          height: 140.0,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: <Widget>[
                              DataContainer(
                                  text: Strings.yourVotesData1,
                                  value: reportsVoted.toString(),
                                  size: size),
                              DataContainer(
                                  text: Strings.yourVotesData2,
                                  value: "$repaired %",
                                  size: size),
                              DataContainer(
                                  text: Strings.yourVotesData3,
                                  value: "$averageRepairTime days",
                                  size: size),
                              DataContainer(
                                  text: Strings.yourVotesData4 +
                                      snapshot.data!.mostVotedCategory,
                                  value: "$mostVotes",
                                  size: size),
                            ],
                          ));
                    } else if (snapshot.hasError) {
                      return GenericFallBack(
                          informativeMessage: snapshot.error.toString());
                    }
                    return const Center(child: CircularProgressIndicator());
                  },
                )
              ],
            )));
  }
}
