import 'package:firebase_auth/firebase_auth.dart';
import 'package:fixup/app/generic_fallback.dart';
import 'package:flutter/material.dart';

import 'package:fixup/blocs/reports_bloc.dart';
import 'package:fixup/blocs/user_bloc.dart';
import 'package:fixup/models/report_model.dart';
import 'package:fixup/app/dashboard/data_circles/data_container.dart';
import 'package:fixup/constants/strings.dart';

class YourReports extends StatefulWidget {
  const YourReports({Key? key}) : super(key: key);

  @override
  _YourReportsState createState() => _YourReportsState();
}

class _YourReportsState extends State<YourReports> {
  final double size = 35;

  @override
  void initState() {
    super.initState();
    User? user = userBloc.getUser();
    reportsBloc.getYourReports(user?.uid);
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Row(
                  children: const [
                    Text(Strings.yourReportsCardTitle,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                  ],
                ),
                StreamBuilder(
                  stream: reportsBloc.dashboardData,
                  builder: (context, AsyncSnapshot<YourReportsModel> snapshot) {
                    if (snapshot.hasData) {
                      int average = snapshot.data!.averageRepairTime.floor();
                      int averageVotes =
                          snapshot.data!.averageVotesReports.floor();
                      int days = snapshot.data!.daysSinceLastReport;
                      int repaired =
                          (snapshot.data!.repairPercentage * 100).ceil();
                      if (average == -1) {
                        return const SizedBox(
                            height: 110.0,
                            child: Text(
                              "To see some information about your reports you need to create one",
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: 20,
                                color: Color(0xff2f3640),
                              ),
                            ));
                      }
                      return SizedBox(
                          height: 140.0,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: <Widget>[
                              DataContainer(
                                  text: Strings.yourReportsData1,
                                  value: snapshot.data!.reportsMade.toString(),
                                  size: size),
                              DataContainer(
                                  text: Strings.yourReportsData2,
                                  value: "$days days",
                                  size: size),
                              DataContainer(
                                  text: Strings.yourReportsData3,
                                  value: "$repaired %",
                                  size: size),
                              DataContainer(
                                  text: Strings.yourReportsData4,
                                  value: "$average days",
                                  size: size),
                              DataContainer(
                                  text: Strings.yourReportsData5,
                                  value: "$averageVotes",
                                  size: size),
                            ],
                          ));
                    } else if (snapshot.hasError) {
                      return GenericFallBack(
                          informativeMessage: snapshot.error.toString());
                    }
                    return const Center(child: CircularProgressIndicator());
                  },
                )
              ],
            )));
  }
}
