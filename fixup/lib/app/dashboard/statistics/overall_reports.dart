import 'package:fixup/app/generic_fallback.dart';
import 'package:fixup/blocs/reports_bloc.dart';
import 'package:fixup/constants/colors.dart';
import 'package:fixup/models/report_model.dart';
import 'package:flutter/material.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import 'package:fixup/app/dashboard/data_circles/data_container.dart';
import 'package:fixup/constants/strings.dart';

class OverallReports extends StatefulWidget {
  const OverallReports({Key? key}) : super(key: key);

  @override
  _OverallReportsState createState() => _OverallReportsState();
}

class _OverallReportsState extends State<OverallReports> {
  bool connection = false;

  @override
  void initState() {
    super.initState();
    reportsBloc.getOverallReports('Bogota');
    _asyncMethod();
  }

  _asyncMethod() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult != ConnectivityResult.mobile &&
        connectivityResult != ConnectivityResult.wifi) {
      connection = false;
    } else {
      connection = true;
    }

    setState(() {});
  }

  Future<bool> checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    super.dispose();
  }

  String dropdownValue = 'Bogota';
  double size = 35;

  @override
  Widget build(BuildContext context) {
    return Card(
        shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(10),
        ),
        child: Padding(
            padding: const EdgeInsets.all(10.0),
            child: Column(
              children: [
                Row(
                  children: [
                    const Text(Strings.overallReportsCardTitle,
                        style: TextStyle(
                            fontWeight: FontWeight.bold, fontSize: 18)),
                    Container(
                      margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                      child: DropdownButton<String>(
                        onTap: () {
                          checkConnectivity().then((value) {
                            if (!value) {
                              showDialog(
                                  context: context,
                                  builder: (context) {
                                    Future.delayed(const Duration(seconds: 5),
                                        () {
                                      Navigator.of(context).pop(true);
                                    });
                                    return const AlertDialog(
                                      title: Text("Can't change city"),
                                      content: Text(
                                          'There is no network connection, you can try again later'),
                                    );
                                  });
                              connection = false;
                              setState(() {});
                            }
                          });
                        },
                        value: dropdownValue,
                        elevation: 16,
                        underline: Container(
                            height: 2, color: const Color(CustomColors.rose)),
                        onChanged: connection
                            ? (String? newValue) {
                                setState(() {
                                  dropdownValue = newValue!;
                                });
                                reportsBloc.getOverallReports(newValue);
                              }
                            : null,
                        items: <String>['Bogota', 'Cali', 'Cartagena']
                            .map<DropdownMenuItem<String>>((String value) {
                          return DropdownMenuItem<String>(
                            value: value,
                            child: Text(value),
                          );
                        }).toList(),
                      ),
                    ),
                  ],
                ),
                StreamBuilder(
                  stream: reportsBloc.overallReports,
                  builder:
                      (context, AsyncSnapshot<OverallReportsModel> snapshot) {
                    if (snapshot.hasData) {
                      int averageVotes = snapshot.data!.averageVotes.floor();
                      int repaired =
                          (snapshot.data!.repairPercentage * 100).ceil();
                      int averageRepairTime =
                          snapshot.data!.averageRepairTime.floor();
                      int votes = snapshot.data!.mostVotedCategoryVotes;
                      String category = snapshot.data!.mostVotedCategory;
                      return SizedBox(
                          height: 140.0,
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: <Widget>[
                              DataContainer(
                                  text: Strings.overallReportsData1,
                                  value: averageVotes.toString(),
                                  size: size),
                              DataContainer(
                                  text: Strings.overallReportsData2,
                                  value: "$repaired %",
                                  size: size),
                              DataContainer(
                                  text: Strings.overallReportsData3,
                                  value: "$averageRepairTime days",
                                  size: size),
                              DataContainer(
                                  text: Strings.overallReportsData4 + category,
                                  value: "$votes",
                                  size: size),
                            ],
                          ));
                    } else if (snapshot.hasError) {
                      return GenericFallBack(
                          informativeMessage: snapshot.error.toString());
                    }
                    return const Center(child: CircularProgressIndicator());
                  },
                )
              ],
            )));
  }
}
