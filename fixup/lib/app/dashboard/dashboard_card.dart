import 'package:fixup/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:cached_network_image/cached_network_image.dart';

class DashboardCard extends StatelessWidget {
  final String reportId;
  final String imageURL;
  final List<Map> cardChildren;

  // ignore: use_key_in_widget_constructors
  const DashboardCard(
      {required this.reportId,
      required this.imageURL,
      required this.cardChildren});

  @override
  Widget build(BuildContext context) {
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(5),
      ),
      child: InkWell(
        onTap: () {},
        child: Column(
          children: [
            ClipRRect(
              borderRadius: BorderRadius.circular(8.0),
              child: CachedNetworkImage(
                  imageUrl: imageURL,
                  errorWidget: (context, url, error) =>
                      Image.asset('assets/images/img-offline-fallback.png'),
                  height: 175),
            ),
            for (var item in cardChildren)
              Padding(
                  padding: const EdgeInsets.all(5.0),
                  child: Row(
                    children: [
                      Icon(item["icon"],
                          color: const Color(CustomColors.darkYellow)),
                      Text(item["text"]),
                    ],
                  ))
          ],
        ),
      ),
    );
  }
}
