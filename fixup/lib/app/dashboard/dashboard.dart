import 'dart:math';
import 'package:fixup/app/generic_fallback.dart';
import 'package:flutter/material.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

import 'package:fixup/blocs/user_bloc.dart';
import 'package:fixup/blocs/reports_bloc.dart';
import 'package:fixup/models/report_model.dart';
import 'package:fixup/constants/colors.dart';
import 'package:fixup/constants/strings.dart';
import 'package:fixup/app/layout_navigation_bar.dart';
import 'package:fixup/app/dashboard/dashboard_card.dart';
import 'package:fixup/app/dashboard/statistics/overall_reports.dart';
import 'package:fixup/app/dashboard/statistics/your_reports.dart';
import 'package:fixup/app/dashboard/statistics/your_votes.dart';

class Dashboard extends StatefulWidget {
  const Dashboard({Key? key}) : super(key: key);

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  @override
  void initState() {
    super.initState();
    User? user = userBloc.getUser();
    reportsBloc.getAllForUser(user?.uid);
    checkConnection();
  }

  @override
  void dispose() {
    //reportsBloc.dispose();
    super.dispose();
  }

  void checkConnection() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    SnackBar snackBar;
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      snackBar = const SnackBar(content: Text('Content updated'));
    } else {
      snackBar = const SnackBar(
          content:
              Text("can't update content, there is no internet connection"));
    }
    ScaffoldMessenger.of(context).showSnackBar(snackBar);
  }

  @override
  Widget build(BuildContext context) {
    Future<void> _pullRefresh() async {
      User? user = userBloc.getUser();
      reportsBloc.getAllForUser(user?.uid);
      reportsBloc.getOverallReports('Bogota');
      reportsBloc.getYourReports(user?.uid);
      reportsBloc.getYourVotes(user?.uid);
      checkConnection();
    }

    var body = RefreshIndicator(
        onRefresh: _pullRefresh,
        child: ListView(
          padding: const EdgeInsets.all(2),
          children: <Widget>[
            Container(
              padding: const EdgeInsets.all(30),
              child: const Text("Your Reports",
                  style: TextStyle(fontWeight: FontWeight.bold, fontSize: 32)),
            ),
            StreamBuilder(
              stream: reportsBloc.userReports,
              builder: (context, AsyncSnapshot<List<ReportModel>> snapshot) {
                if (snapshot.hasData) {
                  if (snapshot.data!.isEmpty) {
                    return Container();
                  } else {
                    return SizedBox(
                      height: 300.0,
                      child: Container(
                          padding: const EdgeInsets.all(2),
                          child: ListView(
                            scrollDirection: Axis.horizontal,
                            children: <Widget>[
                              for (var userReport in snapshot.data!)
                                DashboardCard(
                                    reportId: '0',
                                    imageURL: userReport.media[0].path,
                                    cardChildren: [
                                      {
                                        "icon": Icons.straighten,
                                        "text": Haversine.haversine(
                                                    userReport.damageLocation
                                                        .latitude,
                                                    userReport.damageLocation
                                                        .longitude,
                                                    4.6964142,
                                                    -74.0652501)
                                                .toStringAsFixed(2) +
                                            " Km"
                                      },
                                      {
                                        "icon": Icons.warning,
                                        "text": userReport.severity.toString()
                                      },
                                      {
                                        "icon": Icons.watch_later,
                                        "text": userReport.status
                                      },
                                    ]),
                            ],
                          )),
                    );
                  }
                } else if (snapshot.hasError) {
                  return GenericFallBack(
                      informativeMessage: snapshot.error.toString());
                }
                return const Center(child: CircularProgressIndicator());
              },
            ),
            Container(
              padding: const EdgeInsets.all(2),
              child: const YourReports(),
            ),
            Container(
              padding: const EdgeInsets.all(2),
              child: const OverallReports(),
            ),
            Container(
              padding: const EdgeInsets.all(2),
              child: const YourVotes(),
            )
          ],
        ));
    var colors = [
      CustomColors.grey,
      CustomColors.orange,
      CustomColors.grey,
      CustomColors.grey
    ];

    return LayoutNavBar(
        body: body, appBarText: Strings.dashboardTitle, colors: colors);
  }
}

class Haversine {
  static const R = 6372.8; //In kilometers

  static double haversine(double lat1, lon1, lat2, lon2) {
    double dLat = _toRadians(lat2 - lat1);
    double dLon = _toRadians(lon2 - lon1);

    lat1 = _toRadians(lat1);
    lat2 = _toRadians(lat2);
    double a =
        pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(lat1) * cos(lat2);
    double c = 2 * asin(sqrt(a));
    return R * c;
  }

  static double _toRadians(double degree) {
    return degree * pi / 180;
  }

  static void main() {}
}
