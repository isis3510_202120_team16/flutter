import 'package:fixup/blocs/reports_bloc.dart';
import 'package:fixup/models/report_model.dart';
import 'package:flutter/material.dart';

class Settings extends StatefulWidget {
  const Settings({Key? key}) : super(key: key);

  @override
  _SettingsState createState() => _SettingsState();
}

class _SettingsState extends State<Settings> {
  @override
  void initState() {
    super.initState();
    reportsBloc.getById("E8UnZR6KfYYAFdNUSJZL");
  }

  @override
  void dispose() {
    reportsBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Settings'),
      ),
      body: StreamBuilder(
        stream: reportsBloc.report,
        builder: (context, AsyncSnapshot<ReportModel> snapshot) {
          if (snapshot.hasData) {
            return buildList(snapshot);
          } else if (snapshot.hasError) {
            return Text(snapshot.error.toString());
          }
          return const Center(child: CircularProgressIndicator());
        },
      ),
    );
  }

  Widget buildList(AsyncSnapshot<ReportModel> snapshot) {
    var text = snapshot.data!.category;
    return Padding(padding: EdgeInsets.all(30.0), child: Text(text));
  }
}
