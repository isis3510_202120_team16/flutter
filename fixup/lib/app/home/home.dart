import 'package:flutter/material.dart';

import 'package:fixup/app/home/stream_reports_map.dart';
import 'package:fixup/app/layout_navigation_bar.dart';
import 'package:fixup/constants/colors.dart';
import 'package:fixup/constants/strings.dart';

class HomePage extends StatelessWidget {
  const HomePage({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    var colors = [
      CustomColors.grey,
      CustomColors.grey,
      CustomColors.orange,
      CustomColors.grey
    ];

    var body = const StreamReportsMap();

    return LayoutNavBar(body: body, appBarText: Strings.map, colors: colors);
  }
}
