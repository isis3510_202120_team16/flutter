import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fixup/app/generic_fallback.dart';
import 'package:fixup/constants/strings.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';

import 'package:fixup/app/home/map_view.dart';
import 'package:geolocator/geolocator.dart';

class StreamReportsMap extends StatelessWidget {
  final Function(LatLng)? selectPosition;

  const StreamReportsMap({Key? key, this.selectPosition}) : super(key: key);

  Future getCurrentLocation() async {
    try{
      Location location = Location();
      bool _serviceEnabled;
      PermissionStatus _permissionGranted;
      LocationData _locationData;

      _serviceEnabled = await location.serviceEnabled().timeout(const Duration(seconds: 7));
      if (!_serviceEnabled) {
        _serviceEnabled = await location.requestService().timeout(const Duration(seconds: 7));
        if (!_serviceEnabled) {
          return const LatLng(4.6015, -74.0661);
        }
      }

      _permissionGranted = await location.hasPermission().timeout(const Duration(seconds: 7));
      if (_permissionGranted == PermissionStatus.denied) {
        _permissionGranted = await location.requestPermission().timeout(const Duration(seconds: 7));
        if (_permissionGranted != PermissionStatus.granted) {
          return const LatLng(4.6015, -74.0661);
        }
      }

      _locationData = await location.getLocation();
      return _locationData;
    } catch(error){
      return Future.error(Strings.unknownError);
    }
  }

  Future<LatLng> _determinePosition() async {
    try{
      bool serviceEnabled;
      LocationPermission permission;

      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        throw Exception('Some arbitrary error');
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          throw Exception('Some arbitrary error');
        }
      }

      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      // When we reach here, permissions are granted and we can
      // continue accessing the position of the device.
      Position _locationData = await Geolocator.getCurrentPosition().timeout(const Duration(seconds: 5));
      return LatLng(_locationData.latitude, _locationData.longitude);
    } catch (error){
      Position? position = await Geolocator.getLastKnownPosition();
      if (position == null){
        return const LatLng(4.6015, -74.0661);
      }
      return LatLng(position.latitude, position.longitude);
    }

  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: _determinePosition(),
        builder: (context, AsyncSnapshot snapshot) {
          if (snapshot.hasError) {
            Future.delayed(const Duration(seconds: 3), () {
              getCurrentLocation();
            });
            return const GenericFallBack(
                informativeMessage:
                    "please check your GPS and your internet connection");
          } else if (snapshot.connectionState == ConnectionState.done) {
            if (selectPosition != null) {
              return MapView(
                  zoom: 15.0,
                  initialLatLng:
                      LatLng(snapshot.data.latitude, snapshot.data.longitude),
                  selectPosition: selectPosition);
            } else {
              return MapView(
                  zoom: 15.0,
                  initialLatLng:
                      LatLng(snapshot.data.latitude, snapshot.data.longitude));
            }
          } else if(snapshot.connectionState == ConnectionState.waiting) {
            return const Center(child: CircularProgressIndicator());
          }
          return const GenericFallBack(
              informativeMessage:
              "please check your GPS and your internet connection");

        });
  }
}
