import 'dart:async';
import 'dart:ui';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fixup/blocs/reports_bloc.dart';

import 'package:google_maps_controller/google_maps_controller.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:fixup/app/reports/report_detail.dart';
import 'package:fixup/constants/colors.dart';
import 'package:fixup/models/report_model.dart';
import 'dart:io';

class MapView extends StatefulWidget {
  final double zoom;
  final LatLng initialLatLng;

  final Function(LatLng)? selectPosition;

  const MapView(
      {Key? key,
      required this.zoom,
      required this.initialLatLng,
      this.selectPosition})
      : super(key: key);

  @override
  _MapViewState createState() => _MapViewState();
}

class _MapViewState extends State<MapView> {
  late LatLng userPosition = widget.initialLatLng;
  late GoogleMapsController controller;
  late StreamSubscription<CameraPosition> subscription;
  late StreamSubscription streamSubscription;
  late List<ReportModel> reports;
  LatLngBounds? mapMarginsLimit;
  CameraPosition? position;
  bool toggleValue = false;
  bool mapLoaded = false;
  String mostVotedCategory = "Increase range";
  double distance = 0;
  bool updateDB = true;
  bool snackBarShow = false;
  bool disconnection = false;

  void checkConnection() async {
    SnackBar snackBar;
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult != ConnectivityResult.mobile &&
        connectivityResult != ConnectivityResult.wifi) {
      disconnection = true;
      if (!snackBarShow) {
        snackBar = const SnackBar(
          content: Text("Showing reports near last place with connection"),
          duration: Duration(days: 365),
        );
        ScaffoldMessenger.of(context).showSnackBar(snackBar);
        snackBarShow = true;
      }
      await Future.delayed(const Duration(seconds: 2), () {
        checkConnection();
      });
      // checkConnection();
    } else {
      if (disconnection) {
        ScaffoldMessenger.of(context).hideCurrentSnackBar();
        snackBarShow = false;
        setCirclesOnMapState(refreshPoints: true);
        disconnection = false;
      }
    }
  }

  @override
  void initState() {
    streamSubscription = reportsBloc.reports.listen((data) {
      reports = data;
      checkConnection();
      renderCircles();
    });
    super.initState();
    controller = GoogleMapsController(
      onMapCreated: (GoogleMapController controller) {
        controller.setMapStyle(
            '[{"featureType": "poi","stylers": [{"visibility": "off"}]}]');

        renderSelectPositionMarker();
        setCirclesOnMapState();
        mapLoaded = true;
      },
      myLocationButtonEnabled: false,
      myLocationEnabled: true,
      zoomControlsEnabled: false,
      compassEnabled: true,
      initialCameraPosition: CameraPosition(
        target: widget.initialLatLng,
        zoom: widget.zoom,
      ),
    );

    subscription = controller.onCameraMove$.listen((e) async {
      setState(() {
        position = e;
      });
      renderSelectPositionMarker();
      setCirclesOnMapState();
    });
  }

  void renderSelectPositionMarker() {
    if (widget.selectPosition != null) {
      LatLng markerPosition;
      if (position == null) {
        markerPosition = userPosition;
      } else {
        markerPosition = position!.target;
      }
      widget.selectPosition!(markerPosition);
      controller.clearMarkers();
      controller.addMarker(Marker(
          markerId: MarkerId(markerPosition.toString()),
          position: markerPosition,
          icon: BitmapDescriptor.defaultMarker));
    }
  }

  void locatePosition() {
    setState(() {
      controller.animateCamera(
        CameraUpdate.newCameraPosition(
          CameraPosition(target: userPosition, zoom: widget.zoom),
        ),
      );
    });
  }

  Color colorCircles(ReportModel point) {
    Color color;
    if (point.category == "Road") {
      color = const Color(CustomColors.blue);
    } else if (point.category == "Sidewalk") {
      color = const Color(CustomColors.grenn);
    } else {
      color = const Color(CustomColors.violet);
    }
    return color;
  }

  void renderSmartRadius() {
    controller.addCircle(Circle(
        circleId: const CircleId("circle_id_0"),
        center: userPosition,
        radius: distance,
        strokeWidth: 1,
        fillColor: Colors.orangeAccent.withOpacity(0.3),
        strokeColor: Colors.white));
  }

  setCirclesOnMapState({refreshPoints = false}) async {
    LatLng mapCenterPosition;

    if (position == null) {
      mapCenterPosition = userPosition;
    } else {
      mapCenterPosition = position!.target;
    }

    LatLngBounds? mapMargins = await controller.getVisibleRegion();
    if (mapMarginsLimit == null ||
        !mapMarginsLimit!.contains(mapMargins!.northeast) ||
        !mapMarginsLimit!.contains(mapMargins.southwest)) {
      double offsetLat =
          (mapCenterPosition.latitude - mapMargins!.southwest.latitude).abs();
      double offsetLon =
          (mapCenterPosition.longitude - mapMargins.southwest.longitude).abs();

      mapMarginsLimit = LatLngBounds(
          southwest: LatLng(mapMargins.southwest.latitude - offsetLat,
              mapMargins.southwest.longitude - offsetLon),
          northeast: LatLng(mapMargins.northeast.latitude + offsetLat,
              mapMargins.northeast.longitude + offsetLon));

      if (userPosition.latitude < mapMarginsLimit!.northeast.latitude &&
          userPosition.latitude > mapMarginsLimit!.southwest.latitude &&
          userPosition.longitude < mapMarginsLimit!.northeast.longitude &&
          userPosition.longitude > mapMarginsLimit!.southwest.longitude) {
        updateDB = true;
      } else {
        updateDB = false;
      }

      refreshPoints = true;
    }

    if (refreshPoints) {
      reportsBloc.getReportsBetweenCoordinates(updateDB, mapMarginsLimit);
      updateDB = false;
    }
  }

  // set the state of the circles that are going to be rendered on the screen
  void renderCircles() {
    controller.clearCircles();

    if (toggleValue) {
      setState(() {
        if (reports.isNotEmpty && distance != 0) {
          mostVotedCategory = reports[0].category;
        } else {
          mostVotedCategory = "No reports found in area";
        }
      });
      renderSmartRadius();
    }

    for (ReportModel point in reports) {
      Color color = colorCircles(point);
      LatLng coord =
          LatLng(point.damageLocation.latitude, point.damageLocation.longitude);
      controller.addCircle(Circle(
          circleId: CircleId(point.id),
          center: coord,
          radius: 30,
          strokeWidth: 3,
          fillColor: color,
          strokeColor: Colors.white,
          consumeTapEvents: true,
          onTap: () {
            _onOpenReport(point.id);
          }));
    }
  }

  void _onOpenReport(reportId) {
    showModalBottomSheet<dynamic>(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
              child: Wrap(children: [
            Container(
              margin: const EdgeInsets.all(20),
              padding: MediaQuery.of(context).viewInsets,
              child: ReportDetail(reportId),
            )
          ]));
        });
  }

  Future<bool> checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  @override
  void dispose() {
    subscription.cancel();
    streamSubscription.cancel();
    // reportsBloc.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
          title: const Text("Map"),
          backgroundColor: Colors.white,
          actions: [
            Padding(
                padding: const EdgeInsets.symmetric(horizontal: 20),
                child: Switch.adaptive(
                    activeColor: const Color(CustomColors.grenn),
                    value: toggleValue,
                    onChanged: (value) {
                      if (value) {
                        checkConnectivity().then((connectivity) {
                          if (connectivity) {
                            locatePosition();
                            setState(() {
                              toggleValue = value;
                            });
                          } else {
                            toggleValue = false;
                            showDialog(
                                context: context,
                                builder: (BuildContext context) => AlertDialog(
                                      title: const Center(
                                          child: Text("Connectivity Error")),
                                      content: const Text(
                                          'There is no network connection, try again later'),
                                      actions: <Widget>[
                                        TextButton(
                                            onPressed: () =>
                                                Navigator.pop(context, 'OK'),
                                            child: const Text("OK"))
                                      ],
                                    ));
                          }
                        });
                      } else {
                        distance = 0;
                        mostVotedCategory = "No reports found in area";
                        setState(() {
                          toggleValue = value;
                        });
                        setCirclesOnMapState(refreshPoints: true);
                      }
                    }))
          ]),
      body: Stack(children: <Widget>[
        Column(children: [
          Expanded(
            child: GoogleMaps(
              controller: controller,
            ),
          )
        ]),
        Column(children: [
          Stack(
            children: [
              SizedBox(
                height: MediaQuery.of(context).size.height * 0.2,
              ),
              Padding(
                  padding: EdgeInsets.symmetric(
                      vertical: MediaQuery.of(context).size.width * 0.1),
                  child: Builder(builder: (BuildContext context) {
                    if (toggleValue) {
                      return Container(
                        padding: const EdgeInsets.all(15),
                        color: Colors.white,
                        child: Text(mostVotedCategory,
                            style: const TextStyle(
                                fontSize: 30.0,
                                fontWeight: FontWeight.bold,
                                color: Color(CustomColors.orange),
                                decoration: TextDecoration.none)),
                      );
                    }
                    return Container();
                  })),
            ],
          ),
          SizedBox(
            height: MediaQuery.of(context).size.height * 0.4,
          ),
          Padding(
            padding: EdgeInsets.symmetric(
                horizontal: MediaQuery.of(context).size.width * 0.2),
            child: Builder(builder: (BuildContext context) {
              if (toggleValue) {
                return Slider(
                  value: distance,
                  onChangeEnd: (newDistance) {
                    checkConnectivity().then((connectivity) {
                      if (connectivity) {
                        reportsBloc.getReportsSmartFeature(
                            userPosition, newDistance);
                        setState(() {
                          distance = newDistance;
                        });
                      } else {
                        distance = 0;
                        mostVotedCategory = "No reports found in area";
                        setState(() {
                          toggleValue = false;
                        });
                        setCirclesOnMapState(refreshPoints: true);
                        showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                  title: const Center(
                                      child: Text("Connectivity Error")),
                                  content: const Text(
                                      'There is no network connection, try again later'),
                                  actions: <Widget>[
                                    TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text("OK"))
                                  ],
                                ));
                      }
                    });
                  },
                  onChanged: (newDistance) {
                    renderSmartRadius();
                    setState(() {
                      distance = newDistance;
                    });
                  },
                  min: 0,
                  max: 750.0,
                  label: distance.toString(),
                );
              } else {
                distance = 0;
                mostVotedCategory = "No reports found in area";
                return Container();
              }
            }),
          )
        ])
      ]),
      floatingActionButton: FloatingActionButton(
        heroTag: "btn2",
        onPressed: locatePosition,
        child: const Icon(Icons.navigation, color: Colors.white),
        backgroundColor: const Color(CustomColors.orange),
      ),
    );
  }
}
