import 'package:flutter/material.dart';
import 'package:fixup/app/profile/profile.dart';

class Layout extends StatelessWidget {
  final Widget body;
  final String appBarText;

  const Layout({required this.body, required this.appBarText});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: body,
      appBar: AppBar(
        title: Text(appBarText),
      ),
      drawer: Profile(),
    );
  }
}
