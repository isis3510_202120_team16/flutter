import 'package:fixup/app/home/home.dart';
import 'package:fixup/app/reports/report_create.dart';
import 'package:flutter/material.dart';
import 'package:fixup/app/profile/profile.dart';
import 'package:fixup/app/dashboard/dashboard.dart';
import 'package:fixup/constants/colors.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:location/location.dart';
import 'package:geolocator/geolocator.dart';

class LayoutNavBar extends StatefulWidget {
  final Widget body;
  final String appBarText;
  final List colors;

  const LayoutNavBar(
      {Key? key,
      required this.body,
      required this.appBarText,
      required this.colors})
      : super(key: key);

  @override
  _LayoutNavBarState createState() => _LayoutNavBarState();
}

class _LayoutNavBarState extends State<LayoutNavBar> {
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  Future<LatLng> _determinePosition() async {
    try {
      bool serviceEnabled;
      LocationPermission permission;

      serviceEnabled = await Geolocator.isLocationServiceEnabled();
      if (!serviceEnabled) {
        throw Exception('Some arbitrary error');
      }

      permission = await Geolocator.checkPermission();
      if (permission == LocationPermission.denied) {
        permission = await Geolocator.requestPermission();
        if (permission == LocationPermission.denied) {
          throw Exception('Some arbitrary error');
        }
      }

      if (permission == LocationPermission.deniedForever) {
        return Future.error(
            'Location permissions are permanently denied, we cannot request permissions.');
      }

      // When we reach here, permissions are granted and we can
      // continue accessing the position of the device.
      Position _locationData = await Geolocator.getCurrentPosition()
          .timeout(const Duration(seconds: 5));
      return LatLng(_locationData.latitude, _locationData.longitude);
    } catch (error) {
      try {
        Position? position = await Geolocator.getLastKnownPosition();
        if (position == null) {
          return const LatLng(4.6015, -74.0661);
        }
        return LatLng(position.latitude, position.longitude);
      } catch (error) {
        return const LatLng(4.6015, -74.0661);
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      resizeToAvoidBottomInset: true,
      drawer: Profile(),
      body: widget.body,
      floatingActionButton: FloatingActionButton(
        heroTag: "btn1",
        shape: const RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(20.0))),
        onPressed: () {
          _onButtonPressed();
        },
        child: const Icon(
          Icons.add,
          color: Colors.white,
        ),
        backgroundColor: const Color(CustomColors.orange),
      ),
      floatingActionButtonLocation: FloatingActionButtonLocation.centerDocked,
      bottomNavigationBar: BottomAppBar(
          child: Container(
              height: 80,
              child: Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 20.0),
                  child: Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                          icon: Icon(
                            Icons.person,
                            size: 40,
                            color: Color(widget.colors[0]),
                          ),
                          onPressed: () {
                            if (widget.colors[0] == CustomColors.grey) {
                              _scaffoldKey.currentState!.openDrawer();

                              /**Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => Profile()),
                              );*/
                            }
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.bar_chart,
                              size: 40, color: Color(widget.colors[1])),
                          onPressed: () {
                            if (widget.colors[1] == CustomColors.grey) {
                              ScaffoldMessenger.of(context)
                                  .hideCurrentSnackBar();
                              Navigator.pop(context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Dashboard()),
                              );
                            }
                          },
                        ),
                        const SizedBox.shrink(),
                        IconButton(
                          icon: Icon(Icons.location_pin,
                              size: 40, color: Color(widget.colors[2])),
                          onPressed: () {
                            if (widget.colors[2] == CustomColors.grey) {
                              Navigator.pop(context);
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const HomePage()),
                              );
                            }
                          },
                        ),
                        IconButton(
                          icon: Icon(Icons.notifications,
                              size: 40, color: Color(widget.colors[3])),
                          onPressed: () {},
                        )
                      ])))),
    );
  }

  void _onButtonPressed() {
    showModalBottomSheet<dynamic>(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
              child: Wrap(children: [
            Container(
              margin: const EdgeInsets.all(20),
              padding: MediaQuery.of(context).viewInsets,
              child: FutureBuilder(
                  future: _determinePosition(),
                  builder: (context, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.done ||
                        snapshot.hasData) {
                      return CreateReport(location: snapshot.data);
                    } else {
                      return const Center(child: CircularProgressIndicator());
                    }
                  }),
            )
          ]));
        });
  }
}
