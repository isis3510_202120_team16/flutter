import 'package:flutter/material.dart';
import 'package:fixup/app/layout_navigation_bar.dart';
import 'package:fixup/constants/colors.dart';

class About extends StatelessWidget {
  const About({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    const body = Center(
      child: Text(
        "FixUp v0.0.1",
        textAlign: TextAlign.center,
      ),
    );
    const colors = [
      CustomColors.grey,
      CustomColors.grey,
      CustomColors.grey,
      CustomColors.grey
    ];
    return const LayoutNavBar(body: body, appBarText: "About", colors: colors);
  }
}
