import 'dart:io' as FileW;

import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fixup/models/report_model.dart';
import 'package:flutter/material.dart';
import 'package:fixup/blocs/user_bloc.dart';
import 'package:flutter_cache_manager/file.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:math';
import 'package:share/share.dart';
import 'package:fixup/blocs/reports_bloc.dart';
import 'package:fixup/constants/colors.dart';
import 'package:fixup/app/reports/rating.dart';
import 'package:http/http.dart' as http;
import 'package:cached_network_image/cached_network_image.dart';

class ReportContent extends StatefulWidget {
  AsyncSnapshot<ReportModel> report;
  var currLocation;
  bool location;
  ReportContent(this.report, this.currLocation, this.location);

  @override
  _ReportContentState createState() => _ReportContentState();
}

class _ReportContentState extends State<ReportContent> {
  final double height = 20;
  final double fontSizeLabel = 13;

  Future<bool> checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  Future<Map> getImagePath() async {
    final urlImage = Uri.parse(widget.report.data!.media[0].path);
    final response = await http.get(urlImage);
    final bytes = response.bodyBytes;

    final temp = await getTemporaryDirectory();
    final path = '${temp.path}/image.jpg';
    FileW.File file = FileW.File(path);
    return {"path": path, "file": file, "bytes": bytes};
  }

  @override
  Widget build(BuildContext context) {
    return FutureBuilder(
        future: userBloc.isReportVoted(widget.report.data!.id),
        builder: (context, AsyncSnapshot<bool> isReportSnapshot) {
          var title =
              widget.report.data!.category + " " + widget.report.data!.type;
          if (isReportSnapshot.hasData) {
            return Column(
              children: [
                Row(children: [
                  Text(
                    title,
                    style: const TextStyle(
                      fontSize: 24.0,
                      fontWeight: FontWeight.bold,
                      color: Colors.black,
                    ),
                  ),
                ]),
                SizedBox(height: height),
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Row(
                          children: [
                            Text(
                              widget.report.data!.category,
                              style: TextStyle(
                                  fontSize: 18,
                                  backgroundColor: widget
                                              .report.data!.category ==
                                          "Road"
                                      ? const Color(CustomColors.blue)
                                      : widget.report.data!.category ==
                                              "Sidewalk"
                                          ? const Color(CustomColors.grenn)
                                          : const Color(CustomColors.violet),
                                  color: Colors.white),
                            ),
                            SizedBox(width: 30),
                            widget.location
                                ? Text(Haversine.haversine(
                                            widget.report.data!.damageLocation
                                                .latitude,
                                            widget.report.data!.damageLocation
                                                .longitude,
                                            widget.currLocation.latitude,
                                            widget.currLocation.longitude)
                                        .toStringAsFixed(2) +
                                    " Km")
                                : const Text("")
                          ],
                        )
                      ],
                    ),
                    Column(
                      children: [
                        Row(
                          mainAxisAlignment: MainAxisAlignment.spaceAround,
                          children: [
                            Column(
                              children: [
                                TextButton(
                                    onPressed: () {
                                      checkConnectivity().then((connectivity) {
                                        if (connectivity) {
                                          if (!isReportSnapshot.data!) {
                                            _vote(widget.report.data!.id);
                                          }
                                        } else {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) =>
                                                  AlertDialog(
                                                    title: const Center(
                                                        child: Text(
                                                            "Connectivity Error")),
                                                    content: const Text(
                                                        'There is no network connection, try again later'),
                                                    actions: <Widget>[
                                                      TextButton(
                                                          onPressed: () =>
                                                              Navigator.pop(
                                                                  context,
                                                                  'OK'),
                                                          child:
                                                              const Text("OK"))
                                                    ],
                                                  ));
                                        }
                                      });
                                    },
                                    child: isReportSnapshot.data!
                                        ? const Icon(
                                            Icons.thumb_up_alt_outlined,
                                            color: Colors.grey,
                                          )
                                        : const Icon(
                                            Icons.thumb_up_alt_outlined,
                                            color: Color(CustomColors.orange),
                                            size: 30,
                                          )),
                                Text("(" +
                                    widget.report.data!.numVotes.toString() +
                                    ")"),
                              ],
                            ),
                            Column(
                              children: [
                                TextButton(
                                    onPressed: () {
                                      checkConnectivity().then((connectivity) {
                                        if (connectivity) {
                                          getImagePath().then((map) {
                                            map["file"]
                                                .writeAsBytesSync(map["bytes"]);
                                            Share.shareFiles([map["path"]],
                                                text: "Check for a " +
                                                    widget
                                                        .report.data!.category +
                                                    " " +
                                                    widget.report.data!.type +
                                                    " in this location \nDescription: " +
                                                    widget.report.data!
                                                        .description +
                                                    "\n" +
                                                    "https://maps.google.com/maps?q=${widget.report.data!.damageLocation.latitude}%2C${widget.report.data!.damageLocation.longitude}");
                                          });
                                        } else {
                                          showDialog(
                                              context: context,
                                              builder: (BuildContext context) =>
                                                  AlertDialog(
                                                    title: const Center(
                                                        child: Text(
                                                            "Connectivity Error")),
                                                    content: const Text(
                                                        'There is no network connection, try again later'),
                                                    actions: <Widget>[
                                                      TextButton(
                                                          onPressed: () =>
                                                              Navigator.pop(
                                                                  context,
                                                                  'OK'),
                                                          child:
                                                              const Text("OK"))
                                                    ],
                                                  ));
                                        }
                                      });
                                    },
                                    child: const Icon(Icons.ios_share,
                                        color: Color(CustomColors.orange),
                                        size: 30)),
                                Text("")
                              ],
                            )
                          ],
                        )
                      ],
                    )
                  ],
                ),
                Row(children: [
                  Text(
                    "Severity",
                    style: TextStyle(
                      fontSize: fontSizeLabel,
                      color: Colors.black,
                    ),
                  ),
                  Rating((rating) {}, false, widget.report.data!.severity),
                ]),
                SizedBox(height: height),
                Row(
                  children: [
                    Text(
                      "Status",
                      style: TextStyle(
                        fontSize: fontSizeLabel,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: height * 0.6),
                Text(widget.report.data!.status),
                SizedBox(
                  height: height,
                ),
                Row(children: [
                  Text(
                    "Photo",
                    style: TextStyle(
                      fontSize: fontSizeLabel,
                      color: Colors.black,
                    ),
                  ),
                ]),
                SizedBox(height: height * 0.6),
                Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      CachedNetworkImage(
                          imageUrl: widget.report.data!.media[0].path,
                          errorWidget: (context, url, error) => Image.asset(
                              'assets/images/img-offline-fallback.png'),
                          height: 300,
                          fit: BoxFit.fill)
                    ]),
                SizedBox(
                  height: height,
                ),
                Row(
                  children: [
                    Text(
                      "Description",
                      style: TextStyle(
                        fontSize: fontSizeLabel,
                        color: Colors.black,
                      ),
                    ),
                  ],
                ),
                SizedBox(height: height * 0.6),
                Text(widget.report.data!.description),
              ],
            );
          }
          return Container();
        });
  }

  _vote(id) {
    reportsBloc.voteReport(id);
    userBloc.addVotedReport(id);
    reportsBloc.getById(id);
  }
}

class Haversine {
  static const R = 6372.8; //In kilometers

  static double haversine(double lat1, lon1, lat2, lon2) {
    double dLat = _toRadians(lat2 - lat1);
    double dLon = _toRadians(lon2 - lon1);

    lat1 = _toRadians(lat1);
    lat2 = _toRadians(lat2);
    double a =
        pow(sin(dLat / 2), 2) + pow(sin(dLon / 2), 2) * cos(lat1) * cos(lat2);
    double c = 2 * asin(sqrt(a));
    return R * c;
  }

  static double _toRadians(double degree) {
    return degree * pi / 180;
  }

  static void main() {}
}
