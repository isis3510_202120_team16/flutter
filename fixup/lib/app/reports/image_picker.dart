import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:image_picker/image_picker.dart';

class ImageSelect extends StatefulWidget {
  final int type;

  final Function(File) onImageSelected;

  ImageSelect(this.type, this.onImageSelected);

  @override
  _ImageSelectState createState() => _ImageSelectState();
}

class _ImageSelectState extends State<ImageSelect> {
  File? image;

  Future pickImage() async {
    try {
      ImageSource source =
          widget.type == 0 ? ImageSource.gallery : ImageSource.camera;
      final image = await ImagePicker().pickImage(source: source);
      if (image == null) return;

      final imageTemp = File(image.path);

      setState(() {
        this.image = imageTemp;
      });
      widget.onImageSelected(File(image.path));
    } on PlatformException catch (e) {
      print("Failed to pick image $e");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        children: [
          ElevatedButton(
              onPressed: () => pickImage(),
              child: Icon((widget.type == 1
                  ? (Icons.camera_alt)
                  : (Icons.photo_album))))
        ],
      ),
    );
  }
}
