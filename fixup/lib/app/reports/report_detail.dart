import 'package:fixup/app/reports/report_content.dart';
import 'package:fixup/blocs/user_bloc.dart';

import 'package:fixup/models/report_model.dart';
import 'package:flutter/material.dart';
import 'package:fixup/blocs/reports_bloc.dart';

import 'package:location/location.dart';

class ReportDetail extends StatefulWidget {
  final String reportId;

  ReportDetail(this.reportId);

  @override
  _ReportDetailState createState() => _ReportDetailState();
}

class _ReportDetailState extends State<ReportDetail> {
  late var currLocation;
  bool _isButtonDisabled = false;
  @override
  Future getCurrentLocation() async {
    Location location = new Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();
    return _locationData;
  }

  bool location = false;
  @override
  void initState() {
    getCurrentLocation().then((value) {
      currLocation = value;
      location = true;
      reportsBloc.getById(widget.reportId);
    });
    userBloc.isReportVoted(widget.reportId);
    super.initState();
  }

  final double height = 20;
  final double fontSizeLabel = 13;

  @override
  Widget build(BuildContext context) {
    var body = StreamBuilder(
      stream: reportsBloc.report,
      builder: (context, AsyncSnapshot<ReportModel> snapshot) {
        if (snapshot.hasData) {
          return ReportContent(snapshot, currLocation, location);
        } else if (snapshot.hasError) {
          return Text(snapshot.error.toString());
        }
        return const Center(child: CircularProgressIndicator());
      },
    );

    return Container(child: body);
  }
}
