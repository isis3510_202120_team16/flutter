import 'package:fixup/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:fixup/app/reports/report_create.dart';
import 'package:location/location.dart';

class OpenReport extends StatefulWidget {
  const OpenReport({Key? key}) : super(key: key);

  @override
  _OpenReportState createState() => _OpenReportState();
}

class _OpenReportState extends State<OpenReport> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Row(mainAxisAlignment: MainAxisAlignment.center, children: [
      MaterialButton(
          onPressed: () => {_onButtonPressed()},
          child: Column(
              mainAxisSize: MainAxisSize.min,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisAlignment: MainAxisAlignment.end,
              children: const [
                Icon(Icons.add_box_rounded,
                    color: Color(CustomColors.orange), size: 80)
              ]))
    ]));
  }

  void _onButtonPressed() {
    showModalBottomSheet<dynamic>(
        isScrollControlled: true,
        context: context,
        builder: (context) {
          return SingleChildScrollView(
              child: Wrap(children: [
            Container(
              margin: const EdgeInsets.all(20),
              child: FutureBuilder(
                  future: getCurrentLocation(),
                  builder: (context, AsyncSnapshot snapshot) {
                    if (snapshot.connectionState == ConnectionState.done ||
                        snapshot.hasData) {
                      return CreateReport(location: snapshot.data);
                    }
                    return const Center(child: CircularProgressIndicator());
                  }),
            )
          ]));
        });
  }

  Future getCurrentLocation() async {
    Location location = Location();

    bool _serviceEnabled;
    PermissionStatus _permissionGranted;
    LocationData _locationData;

    _serviceEnabled = await location.serviceEnabled();
    if (!_serviceEnabled) {
      _serviceEnabled = await location.requestService();
      if (!_serviceEnabled) {
        return;
      }
    }

    _permissionGranted = await location.hasPermission();
    if (_permissionGranted == PermissionStatus.denied) {
      _permissionGranted = await location.requestPermission();
      if (_permissionGranted != PermissionStatus.granted) {
        return;
      }
    }

    _locationData = await location.getLocation();
    return _locationData;
  }
}
