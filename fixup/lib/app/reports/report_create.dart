import 'dart:convert';
import 'dart:io';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:fixup/app/home/stream_reports_map.dart';
import 'package:fixup/app/reports/rating.dart';
import 'package:fixup/blocs/reports_bloc.dart';

import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:flutter/material.dart';
import 'package:fixup/constants/strings.dart';
import "package:fixup/app/reports/image_picker.dart";
import 'package:fixup/blocs/user_bloc.dart';
import 'package:path_provider/path_provider.dart';
import 'package:permission_handler/permission_handler.dart'
    as permission_handler;
import 'package:image_gallery_saver/image_gallery_saver.dart';
import 'package:connectivity_plus/connectivity_plus.dart';

class CreateReport extends StatefulWidget {
  final LatLng location;
  const CreateReport({Key? key, required this.location}) : super(key: key);

  @override
  CreateReportState createState() {
    return CreateReportState();
  }
}

class CreateReportState extends State<CreateReport> {
  final _formKey = GlobalKey<FormState>();

  final double height = 20;
  final double fontSizeLabel = 13;
  final descriptionController = TextEditingController();
  @override
  void dispose() {
    // Clean up the controller when the widget is disposed.

    stopwatch.stop();
    descriptionController.dispose();
    super.dispose();
  }

  String dropdownValue = Strings.categoryRoad;
  String dropdownValueType = Strings.typeDamage;
  int _rating = 1;
  File? _image;
  late LatLng locationDamage;
  var textLocation = "Current Location";
  String imagePath = "";
  var reportCache = {
    "category": "Road",
    "severity": 1, //0
    "description": "",
    "latitude": null, //0.0
    "longitude": null, //0.0
    "latitude_damage": null, //0.0,
    "longitude_damage": null, //0.0,
    "type": "Damage",
    "author": "",
    "imagePath": ""
  };

  // ignore: prefer_typing_uninitialized_variables
  late var currLocation;
  late Stopwatch stopwatch;
  bool location = false;
  @override
  void initState() {
    currLocation = widget.location;
    stopwatch = Stopwatch()..start();

    fileReadData().then((data) {
      if (data != null) {
        loadDataCache(data);
      } else {
        locationDamage =
            LatLng(currLocation.latitude!, currLocation.longitude!);
      }
    });
    super.initState();
  }

  loadDataCache(data) {
    _rating = 1;
    setState(() {
      locationDamage =
          LatLng(data["latitude_damage"], data["longitude_damage"]);
      textLocation = "Selected Location";
      _rating = data["severity"];
      descriptionController.text = data["description"];
      dropdownValue = data["category"];
      dropdownValueType = data["type"];
      imagePath = data["imagePath"];
      _image = File(imagePath);
    });
  }

  UserBloc userBloc = UserBloc();
  ReportsBloc reportBloc = ReportsBloc();

  selectPosition(position) {
    locationDamage = position;
  }

  saveFile(File image) async {
    try {
      var response = await ImageGallerySaver.saveFile(image.path);

      imagePath = image.path; //response["filePath"];
    } catch (e) {
      print(e);
    }
  }

  Future<bool> _requestPermission(
      permission_handler.Permission permission) async {
    if (await permission.isGranted) {
      return true;
    } else {
      var result = await permission.request();
      if (result == permission_handler.PermissionStatus.granted) {
        return true;
      } else {
        return false;
      }
    }
  }

  Future<Directory?> documentPath() async {
    if (await _requestPermission(permission_handler.Permission.storage) &&
            await _requestPermission(
                permission_handler.Permission.accessMediaLocation)
        //&& await _requestPermission(permission_handler.Permission.manageExternalStorage)
        ) {
      String tempPath = (await getTemporaryDirectory()).path;
      Directory directory = Directory(tempPath);

      if (!await directory.exists()) {
        await directory.create(recursive: true);
      }
      return directory;
    }
    return null;
  }

  Future<String> filePath() async {
    var document = await documentPath();
    String path = "";
    if (document != null) {
      path = document.path + "/tempReport";
    }
    return path;
  }

  Future fileReadData() async {
    try {
      String _filePath = await filePath();
      if (_filePath != "") {
        File userDocumentFile = File(_filePath);
        final data = await userDocumentFile.readAsString();
        final jsonData = jsonDecode(data);
        return jsonData;
      }
    } catch (e) {
      return null;
    }
  }

  Future<File> writeInFile(data) async {
    String _filePath = await filePath();
    final dataJson = jsonEncode(data);
    File userDocumentFile = File(_filePath);
    return await userDocumentFile.writeAsString(dataJson,
        flush: true, mode: FileMode.write);
  }

  Future clearAllDirectoryItems() async {
    Directory? tempDirectory = (await documentPath());
    if (tempDirectory != null) {
      await tempDirectory.delete(recursive: true);
    }
  }

  Future<bool> checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  saveReport(Map report) async {
    for (var key in report.keys) {
      reportCache[key] = report[key];
    }
    reportCache["imagePath"] = imagePath;
    await writeInFile(reportCache);
  }

  @override
  Widget build(BuildContext context) {
    // Build a Form widget using the _formKey created above.
    //final height = MediaQuery.of(context).size.height;
    //final width = MediaQuery.of(context).size.width;

    return Form(
      key: _formKey,
      child: Column(
        children: <Widget>[
          Row(children: const [
            Text(
              "Create New Report",
              style: TextStyle(
                fontSize: 24.0,
                fontWeight: FontWeight.bold,
                color: Colors.black,
              ),
            ),
          ]),
          SizedBox(height: height),
          Row(children: [
            Text(
              "Location",
              style: TextStyle(
                fontSize: fontSizeLabel,
                color: Colors.black,
              ),
            )
          ]),
          // ignore: prefer_const_literals_to_create_immutables
          Row(mainAxisAlignment: MainAxisAlignment.center, children: [
            IconButton(
                icon: const Icon(Icons.location_on_outlined),
                onPressed: () {
                  showGeneralDialog(
                      context: context,
                      barrierDismissible: true,
                      barrierLabel: MaterialLocalizations.of(context)
                          .modalBarrierDismissLabel,
                      transitionDuration: const Duration(milliseconds: 200),
                      pageBuilder: (BuildContext context, Animation first,
                          Animation second) {
                        return Center(
                          child: SizedBox(
                              width: MediaQuery.of(context).size.width,
                              height: MediaQuery.of(context).size.height,
                              //padding: const EdgeInsets.all(20),
                              child: Stack(children: <Widget>[
                                StreamReportsMap(
                                    selectPosition: (data) =>
                                        selectPosition(data)),
                                Column(children: <Widget>[
                                  SizedBox(
                                    height: MediaQuery.of(context).size.height *
                                        0.9,
                                  ),
                                  Center(
                                      child: ElevatedButton(
                                          child: const Text("Select Position"),
                                          onPressed: () {
                                            Navigator.of(context).pop();
                                            setState(() {
                                              textLocation =
                                                  "Selected Location";
                                            });
                                          }))
                                ]),
                              ])),
                        );
                      });
                }),
            Text(textLocation)
          ]),
          SizedBox(height: height),
          Row(children: [
            Text(
              "Severity",
              style: TextStyle(
                fontSize: fontSizeLabel,
                color: Colors.black,
              ),
            )
          ]),
          Rating((rating) {
            setState(() {
              _rating = rating;
            });
          }, true, _rating),
          SizedBox(height: height),
          Row(
            children: [
              Text(
                "Category",
                style: TextStyle(
                  fontSize: fontSizeLabel,
                  color: Colors.black,
                ),
              ),
            ],
          ),
          SizedBox(height: height * 0.6),
          Row(children: [
            SizedBox(
                height: 42.0,
                width: 320,
                child: DropdownButton<String>(
                  value: dropdownValue,
                  onChanged: (String? newValue) {
                    setState(() {
                      dropdownValue = newValue!;
                    });
                  },
                  isExpanded: true,
                  items: <String>[
                    Strings.categoryRoad,
                    Strings.categorySidewalk,
                    Strings.categoryVandalism
                  ].map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                )),
          ]),
          SizedBox(height: height),
          Row(
            children: [
              Text(
                "Type",
                style: TextStyle(
                  fontSize: fontSizeLabel,
                  color: Colors.black,
                ),
              ),
            ],
          ),
          SizedBox(height: height * 0.6),
          Row(children: [
            SizedBox(
                height: 42.0,
                width: 320,
                child: DropdownButton<String>(
                  value: dropdownValueType,
                  onChanged: (String? newValue) {
                    setState(() {
                      dropdownValueType = newValue!;
                    });
                  },
                  isExpanded: true,
                  items: <String>[Strings.typeDamage, Strings.typeSuggestion]
                      .map<DropdownMenuItem<String>>((String value) {
                    return DropdownMenuItem<String>(
                      value: value,
                      child: Text(value),
                    );
                  }).toList(),
                )),
          ]),
          SizedBox(height: height),
          TextFormField(
            controller: descriptionController,
            decoration: const InputDecoration(
                border: OutlineInputBorder(), hintText: 'Description'),
            // The validator receives the text that the user has entered.
            validator: (value) {
              if (value == null || value.isEmpty) {
                return 'Please enter some text';
              }
              return null;
            },
          ),
          SizedBox(height: height),
          Stack(
            children: [
              _image == null
                  ? const Text("No photo selected")
                  : Column(
                      crossAxisAlignment: CrossAxisAlignment.stretch,
                      children: [
                          Image.file(_image!, height: 300, fit: BoxFit.fill)
                        ]),
              Positioned(
                  top: 0,
                  right: 0,
                  child: IconButton(
                      icon: Icon(
                        Icons.cancel,
                        color: Colors.black.withOpacity(0.5),
                        size: 18,
                      ),
                      onPressed: () => setState(() {
                            _image = null;
                          }))),
            ],
          ),
          SizedBox(height: height),
          Row(mainAxisAlignment: MainAxisAlignment.spaceAround, children: [
            ImageSelect(0, (image) {
              setState(() {
                _image = image;
                imagePath = _image!.path;
              });
            }),
            ImageSelect(1, (image) {
              setState(() {
                _image = image;
                saveFile(_image!);
              });
            }),
          ]),
          SizedBox(height: height),
          SizedBox(
            width: double.infinity,
            child: ElevatedButton(
              onPressed: () {
                // Validate returns true if the form is valid, or false otherwise.
                if (_formKey.currentState!.validate() && _image != null) {
                  // If the form is valid, display a snackbar. In the real world,
                  // you'd often call a server or save the information in a database.

                  if (userBloc.isSIgnedIn()) {
                    User? user = userBloc.getUser();

                    var report = {
                      "category": dropdownValue,
                      "severity": _rating,
                      "description": descriptionController.text,
                      "latitude": currLocation.latitude,
                      "longitude": currLocation.longitude,
                      "latitude_damage": locationDamage.latitude,
                      "longitude_damage": locationDamage.longitude,
                      "type": dropdownValueType,
                      "author": user!.uid,
                      "timer": stopwatch.elapsed.inSeconds
                    };
                    saveReport(report);
                    checkConnectivity().then((connectivity) {
                      if (connectivity) {
                        reportBloc.createReport(report, _image).then((result) {
                          clearAllDirectoryItems();
                          //print(result);
                        });

                        Navigator.pop(context);
                        showDialog(
                            context: context,
                            builder: (context) {
                              Future.delayed(const Duration(seconds: 5), () {
                                Navigator.of(context).pop(true);
                              });
                              return const AlertDialog(
                                title: Text('Report created'),
                                content: Text('Thank you for helping the city'),
                              );
                            });
                      } else {
                        Navigator.pop(context);

                        showDialog(
                            context: context,
                            builder: (BuildContext context) => AlertDialog(
                                  title: const Center(
                                      child: Text("Connectivity Error")),
                                  content: const Text(
                                      'There is no network connection, we saved your report so you can try again later'),
                                  actions: <Widget>[
                                    TextButton(
                                        onPressed: () =>
                                            Navigator.pop(context, 'OK'),
                                        child: const Text("OK"))
                                  ],
                                ));
                      }
                    });
                  } else {
                    ScaffoldMessenger.of(context).showSnackBar(
                        const SnackBar(content: Text('Not signed in ')));
                  }
                }
              },
              child: const Text('Create Report'),
            ),
          )
        ],
      ),
    );
  }
}
