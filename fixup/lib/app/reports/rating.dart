import 'package:fixup/constants/colors.dart';
import 'package:flutter/material.dart';

class Rating extends StatefulWidget {
  final int maximumRating;
  final Function(int) onRatingSelected;
  final bool clickable;
  final int rating;
  Rating(this.onRatingSelected, this.clickable, this.rating,
      [this.maximumRating = 3]);

  @override
  _RatingState createState() => _RatingState();
}

class _RatingState extends State<Rating> {
  late int _currentRating;

  @override
  void initState() {
    super.initState();
    _currentRating = widget.rating;
  }

  Widget _buildRatingCircle(int index) {
    _currentRating = widget.rating;
    if (index < _currentRating) {
      return const Icon(
        Icons.circle,
        color: Color(CustomColors.orange),
        size: 50,
      );
    } else {
      return const Icon(Icons.circle_outlined, size: 50);
    }
  }

  Widget _buildBody() {
    final severity = List<Widget>.generate(widget.maximumRating, (index) {
      return GestureDetector(
        child: _buildRatingCircle(index),
        onTap: () {
          if (widget.clickable) {
            setState(() {
              _currentRating = index + 1;
            });
            widget.onRatingSelected(_currentRating);
          }
        },
      );
    });
    return Container(
        margin: const EdgeInsets.only(
          left: 45,
          right: 45,
        ),
        child: Row(
          children: severity,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
        ));
  }

  @override
  Widget build(BuildContext context) {
    return _buildBody();
  }
}
