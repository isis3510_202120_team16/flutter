import 'package:firebase_auth/firebase_auth.dart';
import 'package:fixup/blocs/user_bloc.dart';
import 'package:flutter/material.dart';

import 'package:fixup/constants/colors.dart';

class ProfileContainer extends StatelessWidget {
  const ProfileContainer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    User? user = userBloc.getUser();
    return Container(
      child: Column(
        children: [
          DrawerHeader(
              child: CircleAvatar(
                  radius: 60.0,
                  backgroundColor: Color(0xffFDCF09),
                  child: CircleAvatar(
                      radius: 55.0,
                      backgroundImage: AssetImage(
                          'assets/images/Avatar-M.png')))), //AssetImage(user!.photoURL!))),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(user!.displayName!),
          ),
          Padding(
            padding: const EdgeInsets.all(10.0),
            child: Text(user.email!),
          )
        ],
      ),
      color: const Color(CustomColors.orange),
    );
  }
}
