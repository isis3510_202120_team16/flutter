import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:fixup/app/login/authentication.dart';
import 'package:fixup/constants/colors.dart';
import 'package:flutter/material.dart';
import 'package:share/share.dart';

import 'package:fixup/app/profile/profile_container.dart';
import 'package:fixup/app/about/about.dart';
import 'package:fixup/app/dashboard/dashboard.dart';
// import 'package:fixup/app/settings/settings.dart';
import 'package:fixup/blocs/user_bloc.dart';

class Profile extends StatefulWidget {
  Profile({Key? key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  final options = ['My reports', 'Settings', 'Invite friends', 'About'];
  final icons = [Icons.report, Icons.settings, Icons.campaign, Icons.book];

  Future<bool> checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        padding: EdgeInsets.zero,
        children: <Widget>[
          const ProfileContainer(),
          ListTile(
            leading: Icon(icons[0], color: const Color(CustomColors.orange)),
            title: Text(options[0]),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const Dashboard()),
              );
            },
          ),
          const Divider(),
          ListTile(
            leading: Icon(icons[2], color: const Color(CustomColors.orange)),
            title: Text(options[2]),
            onTap: () {
              checkConnectivity().then((result) {
                if (result) {
                  Share.share(
                      'Get the best app to report infrastructure damages in your city. Remember the only real damage is the one nobody reports. Download here https://gitlab.com/isis3510_202120_team16/flutter',
                      subject: 'Share FixUp application');
                } else {
                  showDialog(
                      context: context,
                      builder: (BuildContext context) => AlertDialog(
                            title:
                                const Center(child: Text("Connectivity Error")),
                            content: const Text(
                                'There is no network connection, try again later'),
                            actions: <Widget>[
                              TextButton(
                                  onPressed: () => Navigator.pop(context, 'OK'),
                                  child: const Text("OK"))
                            ],
                          ));
                }
              });
            },
          ),
          const Divider(),
          ListTile(
            leading: Icon(icons[3], color: const Color(CustomColors.orange)),
            title: Text(options[3]),
            onTap: () {
              Navigator.pop(context);
              Navigator.push(
                context,
                MaterialPageRoute(builder: (context) => const About()),
              );
            },
          ),
          const Divider(),
          Align(
            alignment: Alignment.bottomCenter,
            child: ElevatedButton(
              onPressed: () {
                userBloc.signOut();
                Navigator.pop(context);
                Navigator.push(
                  context,
                  MaterialPageRoute(builder: (context) => const Auth()),
                );
              },
              child: const Text('Logout'),
            ),
          )
        ],
      ),
    );
  }
}
