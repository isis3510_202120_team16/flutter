import 'package:flutter/material.dart';
import 'package:fixup/app/profile/profile.dart';

class ProfileAvatar extends StatelessWidget {
  const ProfileAvatar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return const Padding(
        padding: EdgeInsets.all(30.0),
        child: CircleAvatar(
          radius: 55.0,
          backgroundColor: Colors.orange,
          child: Text('Profile'),
        ));
  }
}
