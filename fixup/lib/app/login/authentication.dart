import 'package:firebase_auth/firebase_auth.dart';
import 'package:fixup/app/home/home.dart';
import 'package:fixup/app/login/login.dart';
import 'package:fixup/blocs/user_bloc.dart';
import 'package:flutter/cupertino.dart';

class Auth extends StatefulWidget {
  const Auth({Key? key}) : super(key: key);

  @override
  _AuthState createState() => _AuthState();
}

class _AuthState extends State<Auth> {
  User? user;

  @override
  void initState() {
    super.initState();

    authenticated(userBloc.getUser());
  }

  authenticated(userCred) {
    setState(() {
      user = userCred;
    });
  }

  signOut() {
    userBloc.signOut();
    setState(() {
      user = userBloc.getUser();
    });
  }

  @override
  Widget build(BuildContext context) {
    if (user == null) {
      return Login(onSignIn: (userCred) => authenticated(userCred));
    } else {
      return const HomePage();
    }
  }
}
