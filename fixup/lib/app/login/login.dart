import 'package:firebase_auth/firebase_auth.dart';
import 'package:fixup/app/login/signup.dart';
import 'package:fixup/blocs/user_bloc.dart';
import 'package:fixup/constants/colors.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';

class Login extends StatefulWidget {
  final Function(User?) onSignIn;
  const Login({Key? key, required this.onSignIn}) : super(key: key);

  @override
  _LoginState createState() => _LoginState();
}

class _LoginState extends State<Login> {
  late TextEditingController _controller1;
  late TextEditingController _controller2;
  @override
  void initState() {
    super.initState();
    _controller1 = TextEditingController();
    _controller2 = TextEditingController();
  }

  @override
  void dispose() {
    _controller1.dispose();
    _controller2.dispose();
    super.dispose();
  }

  login() async {
    try {
      UserCredential userCred =
          await userBloc.signInUser(_controller1.text, _controller2.text);
      widget.onSignIn(userCred.user);
    } on FirebaseAuthException catch (e) {
      return Future.error(e.message.toString());
    }
  }

  displayAlert(String error, context) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: const Center(child: Text("Login Error")),
              content: Text(error),
              actions: <Widget>[
                TextButton(
                    onPressed: () => Navigator.pop(context, 'OK'),
                    child: const Text("OK"))
              ],
            ));
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return Container(
      decoration: const BoxDecoration(
        image: DecorationImage(
          image: AssetImage("assets/images/login.jpg"),
          fit: BoxFit.cover,
        ),
      ),
      child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
        SizedBox(
          height: height * 0.27,
        ),
        Padding(
            padding: EdgeInsets.symmetric(horizontal: width * 0.05),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                const Text("LOGIN",
                    style: TextStyle(
                        fontSize: 30.0,
                        fontWeight: FontWeight.w100,
                        color: Color(CustomColors.orange),
                        decoration: TextDecoration.none)),
                const SizedBox(
                  height: 15,
                ),
                const Text(
                  "Email",
                  style: TextStyle(
                      fontSize: 18,
                      color: Color(CustomColors.grey),
                      fontWeight: FontWeight.w100,
                      decoration: TextDecoration.none),
                ),
                Container(
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Color(CustomColors.grey)))),
                  child: Material(
                    child: TextField(
                      controller: _controller1,
                      style: const TextStyle(fontSize: 20),
                      decoration: const InputDecoration(
                          fillColor: Color(CustomColors.rose),
                          filled: true,
                          border: InputBorder.none,
                          hintStyle:
                              TextStyle(color: Color(CustomColors.grey))),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 30,
                ),
                Container(
                  child: const Text(
                    "Password",
                    style: TextStyle(
                        fontSize: 18,
                        color: Color(CustomColors.grey),
                        fontWeight: FontWeight.w100,
                        decoration: TextDecoration.none),
                  ),
                ),
                Container(
                  decoration: const BoxDecoration(
                      border: Border(
                          bottom: BorderSide(color: Color(CustomColors.grey)))),
                  child: Material(
                    child: TextField(
                      controller: _controller2,
                      obscureText: true,
                      style: const TextStyle(fontSize: 20),
                      decoration: const InputDecoration(
                          fillColor: Color(CustomColors.rose),
                          filled: true,
                          border: InputBorder.none,
                          hintStyle: TextStyle(
                              color: Color(CustomColors.grey), fontSize: 30)),
                    ),
                  ),
                ),
                const SizedBox(
                  height: 15,
                ),
                Container(
                  alignment: Alignment.centerRight,
                  child: const Text(
                    "Forgot Password?",
                    style: TextStyle(
                        fontSize: 15,
                        color: Color(CustomColors.grey),
                        fontWeight: FontWeight.w100,
                        decoration: TextDecoration.none),
                  ),
                ),
                SizedBox(
                  height: height * 0.05,
                ),
                Center(
                    child: ElevatedButton(
                  style: ElevatedButton.styleFrom(
                      fixedSize: Size(width * 0.9, height * 0.06),
                      textStyle: const TextStyle(fontSize: 20)),
                  child: const Text("LOGIN"),
                  onPressed: () {
                    // print("Email:" + _controller1.text);
                    // print("Password:" + _controller2.text);
                    login().catchError((e) {
                      displayAlert(e, context);
                    });
                  },
                )),
                SizedBox(height: height * 0.15),
                Center(
                  child: RichText(
                      text: TextSpan(
                    children: [
                      const TextSpan(
                        text: "Don't have an account? ",
                        style: TextStyle(
                            fontSize: 15,
                            color: Color(CustomColors.grey),
                            fontWeight: FontWeight.w100,
                            decoration: TextDecoration.none),
                      ),
                      TextSpan(
                        text: "Sign up",
                        style: const TextStyle(
                            fontSize: 15,
                            color: Color(CustomColors.blue),
                            fontWeight: FontWeight.w100,
                            decoration: TextDecoration.none),
                        recognizer: new TapGestureRecognizer()
                          ..onTap = () {
                            Navigator.push(
                              context,
                              MaterialPageRoute(
                                  builder: (context) => SignUp(
                                      onSignIn: (userCred) =>
                                          widget.onSignIn(userCred))),
                            );
                          },
                      ),
                    ],
                  )),
                )
              ],
            ))
      ]),
    );
  }
}
