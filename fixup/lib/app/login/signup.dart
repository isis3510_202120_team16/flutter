import 'dart:async';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:fixup/app/login/authentication.dart';
import 'package:fixup/blocs/user_bloc.dart';
import 'package:fixup/constants/colors.dart';
import 'package:connectivity_plus/connectivity_plus.dart';
import 'package:flutter/gestures.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class SignUp extends StatefulWidget {
  final Function(User?) onSignIn;
  const SignUp({Key? key, required this.onSignIn}) : super(key: key);

  @override
  _SignUpState createState() => _SignUpState();
}

class _SignUpState extends State<SignUp> {
  late TextEditingController _controller_firstName;
  late TextEditingController _controller_lastName;
  late TextEditingController _controller_email;
  late TextEditingController _controller_password;
  late TextEditingController _controller_passwordRepeat;

  bool female = true;

  Future<void> signUp() async {
    try {
      UserCredential userCred = await userBloc.signUpUser(
          _controller_email.text, _controller_password.text);

      User user = userCred.user!;
      await user.updatePhotoURL(female ? "female" : "male");
      await user.updateDisplayName(
          _controller_firstName.text + " " + _controller_lastName.text);
      await user.reload();
      user = userBloc.getUser()!;
      setState(() {
        widget.onSignIn(userCred.user);
      });
    } on FirebaseAuthException catch (e) {
      return Future.error(e.message.toString());
    }
  }

  displayAlert(String error, context) {
    showDialog(
        context: context,
        builder: (BuildContext context) => AlertDialog(
              title: const Center(child: Text("Sign Up Error")),
              content: Text(error),
              actions: <Widget>[
                TextButton(
                    onPressed: () => Navigator.pop(context, 'OK'),
                    child: const Text("OK"))
              ],
            ));
  }

  @override
  void initState() {
    super.initState();
    _controller_firstName = TextEditingController();
    _controller_lastName = TextEditingController();
    _controller_email = TextEditingController();
    _controller_password = TextEditingController();
    _controller_passwordRepeat = TextEditingController();
    getPreferences();
  }

  Future<bool> checkConnectivity() async {
    var connectivityResult = await (Connectivity().checkConnectivity());
    if (connectivityResult == ConnectivityResult.mobile ||
        connectivityResult == ConnectivityResult.wifi) {
      return true;
    } else {
      return false;
    }
  }

  savePreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setString("firstName", _controller_firstName.text);
    await prefs.setString("lastName", _controller_lastName.text);
    await prefs.setString("email", _controller_email.text);
    await prefs.setBool("female", female);
  }

  getPreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    setState(() {
      _controller_firstName.text =
          prefs.containsKey("firstName") ? prefs.getString("firstName")! : "";
      _controller_lastName.text =
          prefs.containsKey("lastName") ? prefs.getString("lastName")! : "";
      _controller_email.text =
          prefs.containsKey("email") ? prefs.getString("email")! : "";
      female = prefs.containsKey("female") ? prefs.getBool("female")! : true;
    });
  }

  removePreferences() async {
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove("firstName");
    await prefs.remove("lastName");
    await prefs.remove("email");
    await prefs.remove("female");
  }

  @override
  Widget build(BuildContext context) {
    final height = MediaQuery.of(context).size.height;
    final width = MediaQuery.of(context).size.width;
    return SingleChildScrollView(
      child: Container(
        decoration: const BoxDecoration(
            image: DecorationImage(
              image: AssetImage("assets/images/sign_in.png"),
              fit: BoxFit.cover,
            ),
            color: Colors.white),
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
          SizedBox(
            height: height * 0.14,
          ),
          Padding(
              padding: EdgeInsets.symmetric(horizontal: width * 0.05),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text("SIGN UP",
                      style: TextStyle(
                          fontSize: height * 0.03,
                          fontWeight: FontWeight.w100,
                          color: const Color(CustomColors.orange),
                          decoration: TextDecoration.none)),
                  SizedBox(
                    height: height * 0.05,
                  ),
                  Text(
                    "First Name",
                    style: TextStyle(
                        fontSize: height * 0.025,
                        color: const Color(CustomColors.grey),
                        fontWeight: FontWeight.w100,
                        decoration: TextDecoration.none),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(color: Color(CustomColors.grey)))),
                    child: Material(
                      child: TextField(
                        controller: _controller_firstName,
                        style: const TextStyle(fontSize: 20),
                        decoration: const InputDecoration(
                            fillColor: Color(CustomColors.rose),
                            filled: true,
                            border: InputBorder.none,
                            hintStyle:
                                TextStyle(color: Color(CustomColors.grey))),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.030,
                  ),
                  Text(
                    "Last Name",
                    style: TextStyle(
                        fontSize: height * 0.025,
                        color: const Color(CustomColors.grey),
                        fontWeight: FontWeight.w100,
                        decoration: TextDecoration.none),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(color: Color(CustomColors.grey)))),
                    child: Material(
                      child: TextField(
                        controller: _controller_lastName,
                        style: const TextStyle(fontSize: 20),
                        decoration: const InputDecoration(
                            fillColor: Color(CustomColors.rose),
                            filled: true,
                            border: InputBorder.none,
                            hintStyle: TextStyle(
                                color: Color(CustomColors.grey), fontSize: 30)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.025,
                  ),
                  Text(
                    "Gender",
                    style: TextStyle(
                        fontSize: height * 0.025,
                        color: const Color(CustomColors.grey),
                        fontWeight: FontWeight.w100,
                        decoration: TextDecoration.none),
                  ),
                  Center(
                      child: Row(
                    children: [
                      OutlinedButton(
                          onPressed: () {
                            setState(() {
                              female = true;
                            });
                          },
                          style: ElevatedButton.styleFrom(
                              primary: female
                                  ? Colors.grey.withOpacity(0.5)
                                  : Colors.grey.withOpacity(0.2),
                              fixedSize: Size(width * 0.45, height * 0.01),
                              textStyle: TextStyle(
                                  fontSize: height * 0.02,
                                  color: Colors.black)),
                          child: const Text("Female",
                              style: TextStyle(
                                color: Color(CustomColors.black),
                              ))),
                      OutlinedButton(
                          onPressed: () {
                            setState(() {
                              female = false;
                            });
                          },
                          style: ElevatedButton.styleFrom(
                              primary: female
                                  ? Colors.grey.withOpacity(0.2)
                                  : Colors.grey.withOpacity(0.5),
                              fixedSize: Size(width * 0.45, height * 0.01),
                              textStyle: TextStyle(
                                  fontSize: height * 0.02,
                                  color: Colors.black)),
                          child: const Text("Male",
                              style: TextStyle(
                                color: Color(CustomColors.black),
                              )))
                    ],
                  )),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "Email",
                    style: TextStyle(
                        fontSize: height * 0.025,
                        color: const Color(CustomColors.grey),
                        fontWeight: FontWeight.w100,
                        decoration: TextDecoration.none),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(color: Color(CustomColors.grey)))),
                    child: Material(
                      child: TextField(
                        controller: _controller_email,
                        style: const TextStyle(fontSize: 20),
                        decoration: const InputDecoration(
                            fillColor: Color(CustomColors.rose),
                            filled: true,
                            border: InputBorder.none,
                            hintStyle: TextStyle(
                                color: Color(CustomColors.grey), fontSize: 30)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "Password",
                    style: TextStyle(
                        fontSize: height * 0.025,
                        color: const Color(CustomColors.grey),
                        fontWeight: FontWeight.w100,
                        decoration: TextDecoration.none),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(color: Color(CustomColors.grey)))),
                    child: Material(
                      child: TextField(
                        controller: _controller_password,
                        obscureText: true,
                        style: const TextStyle(fontSize: 20),
                        decoration: const InputDecoration(
                            fillColor: Color(CustomColors.rose),
                            filled: true,
                            border: InputBorder.none,
                            hintStyle: TextStyle(
                                color: Color(CustomColors.grey), fontSize: 30)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Text(
                    "Repeat Password",
                    style: TextStyle(
                      fontSize: height * 0.025,
                      color: const Color(CustomColors.grey),
                      fontWeight: FontWeight.w100,
                      decoration: TextDecoration.none,
                    ),
                  ),
                  Container(
                    decoration: const BoxDecoration(
                        border: Border(
                            bottom:
                                BorderSide(color: Color(CustomColors.grey)))),
                    child: Material(
                      child: TextField(
                        controller: _controller_passwordRepeat,
                        obscureText: true,
                        style: const TextStyle(fontSize: 20),
                        decoration: const InputDecoration(
                            fillColor: Color(CustomColors.rose),
                            filled: true,
                            border: InputBorder.none,
                            hintStyle: TextStyle(
                                color: Color(CustomColors.grey), fontSize: 30)),
                      ),
                    ),
                  ),
                  SizedBox(
                    height: height * 0.04,
                  ),
                  Center(
                      child: ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              fixedSize: Size(width * 0.9, height * 0.06),
                              textStyle: const TextStyle(fontSize: 20)),
                          child: const Text("SIGN UP"),
                          onPressed: () {
                            checkConnectivity().then((connectivity) {
                              if (connectivity) {
                                if (_controller_password.text ==
                                    _controller_passwordRepeat.text) {
                                  signUp().then((_) {
                                    removePreferences();
                                    Navigator.push(
                                      context,
                                      MaterialPageRoute(
                                          builder: (context) => const Auth()),
                                    );
                                  }).catchError((e) {
                                    displayAlert(e, context);
                                  });
                                } else {
                                  displayAlert(
                                      "The passwords don't match", context);
                                  _controller_password.text = "";
                                  _controller_passwordRepeat.text = "";
                                }
                              } else {
                                savePreferences();
                                displayAlert("There is no connection", context);
                              }
                            });
                          })),
                  SizedBox(
                    height: height * 0.02,
                  ),
                  Center(
                    child: RichText(
                        text: TextSpan(
                      children: [
                        const TextSpan(
                          text: "Already have an account? ",
                          style: TextStyle(
                              fontSize: 15,
                              color: Color(CustomColors.grey),
                              fontWeight: FontWeight.w100,
                              decoration: TextDecoration.none),
                        ),
                        TextSpan(
                          text: "Login",
                          style: const TextStyle(
                              fontSize: 15,
                              color: Color(CustomColors.blue),
                              fontWeight: FontWeight.w100,
                              decoration: TextDecoration.none),
                          recognizer: TapGestureRecognizer()
                            ..onTap = () {
                              Navigator.push(
                                context,
                                MaterialPageRoute(
                                    builder: (context) => const Auth()),
                              );
                            },
                        ),
                      ],
                    )),
                  ),
                  SizedBox(
                    height: height * 0.05,
                  ),
                ],
              ))
        ]),
      ),
    );
  }
}
