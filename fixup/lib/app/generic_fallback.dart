import 'package:flutter/material.dart';

class GenericFallBack extends StatelessWidget {
  final String informativeMessage;
  const GenericFallBack({Key? key, required this.informativeMessage})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Center(
      child: AspectRatio(
        aspectRatio: 100 / 100,
        child: Container(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Padding(
                padding: const EdgeInsets.symmetric(vertical: 30),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: [
                    Image.asset('assets/images/img-offline-logo.png'),
                  ],
                ),
              ),
              Expanded(
                child: Text(
                  informativeMessage,
                  textAlign: TextAlign.center,
                  style: const TextStyle(
                    fontSize: 20,
                    color: Color(0xff2f3640),
                  ),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
