import 'package:flutter/material.dart';

class CustomColors {
  // Primary color palette
  static const orange = 0xfffc6643;
  static const rose = 0xfff5d7cf;
  static const lightGrey = 0xfff5f5f5;
  static const grey = 0xff8a8a8a;
  static const black = 0xff000000;

  // Secondary color palette
  static const blue = 0xff51a9ee;
  static const darkYellow = 0xfff0a72d;
  static const grenn = 0xff6eca97;
  static const violet = 0xffb862ec;
  static const yellow = 0xffb862ec;
}

class Palette {
  static const MaterialColor orangeToLight = MaterialColor(
    0xfffc6643, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
    <int, Color>{
      50: Color(0xfffc7556), //10%
      100: Color(0xfffd8569), //20%
      200: Color(0xfffd947b), //30%
      300: Color(0xfffda38e), //40%
      400: Color(0xfffeb3a1), //50%
      500: Color(0xfffec2b4), //60%
      600: Color(0xfffed1c7), //70%
      700: Color(0xfffee0d9), //80%
      800: Color(0xfffff0ec), //90%
      900: Color(0xffffffff), //100%
    },
  );
}
