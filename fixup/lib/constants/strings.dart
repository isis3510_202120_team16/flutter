class Strings {
  // Generic strings
  static const String ok = 'OK';
  static const String cancel = 'Cancel';

  // Logout
  static const String logout = 'Logout';
  static const String logoutAreYouSure =
      'Are you sure that you want to logout?';
  static const String logoutFailed = 'Logout failed';

  // Sign In Page
  static const String signIn = 'Sign in';
  static const String signInWithEmailPassword =
      'Sign in with email and password';
  static const String or = 'or';
  static const String signInFailed = 'Sign in failed';

  // Home page
  static const String map = 'Map Home';

  // Report create page
  static const String reportCreate = 'New Report';

  // Report detail page
  static const String reportDetail = 'Report';

  // Report types
  static const String typeSuggestion = 'Suggestion';
  static const String typeDamage = 'Damage';

  // Report categories
  static const String categoryRoad = 'Road';
  static const String categorySidewalk = 'Sidewalk';
  static const String categoryVandalism = 'Vandalism';

  // profile
  static const String profile = 'Profile';

  // Dashboard
  static const String dashboardTitle = 'Your Reports';

  static const String overallReportsCardTitle = 'Overall Reports';
  static const String overallReportsData1 = 'Average votes per repair';
  static const String overallReportsData2 = 'Repaired';
  static const String overallReportsData3 = 'Average repair wait time';
  static const String overallReportsData4 = 'severe reports ';

  static const String yourReportsCardTitle = 'Your Reports';
  static const String yourReportsData1 = 'Reports made';
  static const String yourReportsData2 = 'Since last report';
  static const String yourReportsData3 = 'Repaired';
  static const String yourReportsData4 = 'Average repair wait time';
  static const String yourReportsData5 = 'Average votes per report';

  static const String yourVotesCardTitle = 'Your Votes';
  static const String yourVotesData1 = 'Reports voted';
  static const String yourVotesData2 = 'Repaired';
  static const String yourVotesData3 = 'Average repair wait time';
  static const String yourVotesData4 = 'most voted ';

  // error messages
  static const String daoDashboardError =
      "Can't get your stats, there is not internet connection";
  static const String unknownError = "Check your connection and try again";
}
